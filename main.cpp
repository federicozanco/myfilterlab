#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#include <cmath>
#endif

#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>

using namespace cv;
using namespace std;

/** Global variables */
Mat roi;
int max_trackbar = 255;

int cursor1 = 0;
int cursor2 = 3;
int cursor3 = 2;
int cursor4 = 64;
int cursor5 = 0;
int cursor6 = 0;

double calcAngle(Point &a, Point &b)
{
    double dx = a.x - b.x;
    double dy = a.y - b.y;

    return acos((dx * dx - dy * dy) / (dx * dx + dy * dy)) / 2.0;

    if (abs(dx) > 0)
        return (double ) dy / (double ) dx;
    else
    {
        if (a.y > b.y)
            return -1000.0F;
        else
            return 1000.0F;
    }
}

double calcSlope(Point &a, Point &b)
{
    int dx = a.x - b.x;
    int dy = a.y - b.y;

    if (abs(dx) > 0)
        return (double ) dy / (double ) dx;
    else
    {
        if (a.y > b.y)
            return -1000.0F;
        else
            return 1000.0F;
    }
}

void print_values(Mat &src)
{
    for (int i = 0; i < src.size().height; i++)
    {
        for (int j = 0; j < src.size().width; j++)
            printf("\t%d", src.at<uchar>(i, j));
        printf("\n");
    }
}

void print_values_s(Mat &src)
{
    for (int i = 0; i < src.size().height; i++)
    {
        for (int j = 0; j < src.size().width; j++)
            printf("\t%d", src.at<short>(i, j));
        printf("\n");
    }
}

void print_histogram(Mat &src)
{
    int hist[256];
    for (int i = 0; i < 256; i++)
        hist[i] = 0;

    for (int r = 0; r < src.rows; r++)
    {
        uchar *srow = src.ptr(r);

        for (int c = 0; c < src.cols; c++)
        {
            int g = (int ) srow[c];

            hist[g]++;
        }
    }

    int maxc = 0;

    for (int i = 0; i < 256; i++)
    {
        if (hist[i] > maxc)
            maxc = hist[i];
    }

    double f = 100.0 / (double ) maxc;

    for (int i = 0; i < 256; i++)
    {
        printf("%i\t", i);
        int l = (int ) ((double ) hist[i] * f);
        for (int s = 0; s < l ; s++)
            printf("*");

        printf(" %i\n", hist[i]);
    }
}

Mat mat2gray(const Mat& src)
{
    Mat flt;
    normalize(src, flt, 0.0, 1.0, NORM_MINMAX);
    return flt;
}

bool sign(int x)
{
    return (x >= 0) ? true : false;
}

int absdiff(int a, int b)
{
    if (a > b)
        return a - b;
    else
        return b - a;
}

Mat sd(Mat src, int ksize)
{
    if (ksize <= 0)
        ksize = 1;
    /*
   Mat image = src.clone();

   Mat image32f;
   image.convertTo(image32f, CV_32F);*/

    Mat image32f;
    cvtColor( src, image32f, COLOR_RGB2GRAY, 1 );
    image32f.convertTo(image32f, CV_32F);

    Mat mu;
    blur(image32f, mu, Size(ksize, ksize));

    Mat mu2;
    blur(image32f.mul(image32f), mu2, Size(ksize, ksize));

    Mat sigma;
    cv::sqrt(mu2 - mu.mul(mu), sigma);

    Mat gray32f = mat2gray(sigma);
    imshow("sigma", gray32f);

    Mat gray;
    sigma.convertTo(gray, CV_8UC1);
    //cvtColor( gray, gray, COLOR_RGB2GRAY, 1 );
    threshold(gray, gray, 2, 255, THRESH_BINARY);
    imshow("sigma binary", gray);
    return gray;
}

Mat binary(Mat src, int thresh1, int thresh2, int bKsize)
{
    Mat gray;
    Mat flt;

    cvtColor(src, gray, COLOR_RGB2GRAY);

    //equalizeHist(gray, gray);

    if (bKsize % 2 == 0)
        bKsize++;

    //medianBlur(gray, flt, bKsize);
    //GaussianBlur(gray, flt, Size(bKsize, bKsize), 0.5, 0.5);
    flt = gray.clone();

    threshold(flt, flt, thresh1, 255, THRESH_TOZERO);
    threshold(flt, flt, thresh1 + thresh2, 255, THRESH_TOZERO_INV);
    //threshold(flt, flt, thresh1, 255, THRESH_BINARY);

    imshow("gray", gray);
    imshow("flt", flt);

    return flt;
}

Mat binaryInv(Mat src, int thresh1, int thresh2, int bKsize)
{
    Mat gray;
    Mat flt;

    cvtColor(src, gray, COLOR_RGB2GRAY);

    //equalizeHist(gray, gray);

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, flt, bKsize);

    namedWindow( "gray", WINDOW_AUTOSIZE );
    imshow("gray", flt);

    Mat a, b;

    threshold(flt, a, thresh1, 255, THRESH_BINARY);
    threshold(flt, b, thresh2, 255, THRESH_BINARY_INV);

    flt = a + b;

    //dilate(flt, flt, Mat(), Point(0, 0), 2);
    //erode(flt, flt, Mat(), Point(0, 0), 3);
    //threshold(255 - flt, flt, thresh1, 255, THRESH_BINARY);

    imshow("flt", flt);

    return flt;
}

Mat binaryOTSU(Mat src, int bKsize)
{
    Mat gray;
    Mat flt;

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, flt, bKsize);

    threshold(flt, flt, 0, 255, THRESH_BINARY | CV_THRESH_OTSU);

    imshow("flt", flt);

    return flt;
}

Mat minmax(Mat src , int thres)
{
    Mat gray;

    cvtColor(src, gray, COLOR_RGB2GRAY);

    Mat er;
    erode(gray, er, Mat());

    Mat dil;
    dilate(gray, dil, Mat());

    Mat d1 = er - gray + 50;
    Mat d2 = gray - dil + 50;
    Mat d3 = d1 + d2 / 2;
    Mat d4;
    threshold(d3, d4, thres, 255, THRESH_BINARY_INV);

    namedWindow( "d1", WINDOW_AUTOSIZE );
    imshow( "d1", d1 );

    namedWindow( "d2", WINDOW_AUTOSIZE );
    imshow( "d2", d2 );

    namedWindow( "d3", WINDOW_AUTOSIZE );
    imshow( "d3", d3  );

    namedWindow( "d4", WINDOW_AUTOSIZE );
    imshow( "d4", d4  );

    return d4;
}

Mat minmax2(Mat src , int iterations, int ksize)
{
    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    ksize = ksize * 2 + 1;

    GaussianBlur(gray, gray, Size(5, 5), 2, 2);

    Mat gaussian = gray.clone();
    namedWindow("gaussian", WINDOW_AUTOSIZE);
    imshow("gaussian", gaussian);

    int nr = src.rows;
    int nc = src.cols;

    Mat element = getStructuringElement(MORPH_RECT, Size(ksize, ksize));
    Mat_<uchar> flt = gray;
    Mat dil;
    Mat er;
    Mat ddil;
    Mat der;

    for (int i = 0; i < iterations; i++)
    {
        gray = flt;
        erode(gray, er, element);
        dilate(gray, dil, element);

        ddil = dil - gray;
        der = gray - er;

        uchar *dptr = flt.ptr(0);
        uchar *dilptr = dil.ptr(0);
        uchar *erptr = er.ptr(0);
        uchar *ddilptr = ddil.ptr(0);
        uchar *derptr = der.ptr(0);

        int p = 0;

        for (int r = 0; r < nr; r++)
        {
            for (int c = 0; c < nc; c++)
            {
                if (derptr[p] < ddilptr[p])
                    dptr[p] = erptr[p];
                else if (derptr[p] > ddilptr[p])
                    dptr[p] = dilptr[p];

                p++;
            }
        }
    }

    imshow("flt", flt);

    return flt;
}

Mat laplacianop(Mat src, int ksize , int bKsize, int fltThres, int laplThres)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);
    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (ksize % 2 == 0)
        ksize++;

    if (bKsize % 2 == 0)
        bKsize++;

    if (ksize < 1)
    {
        threshold(gray, gray, 125, 255, THRESH_BINARY);
        return gray;
    }

    medianBlur(gray, gray, bKsize);

    //GaussianBlur(gray, gray, Size(ksize, ksize), 1);

    Mat lapl;
    Laplacian(gray, lapl, CV_16S, ksize);
    //print_values_s(lapl);

    add(gray, lapl, flt, noArray(), CV_8U);
    threshold(flt, flt, fltThres, 255, THRESH_BINARY_INV);
    imshow( "flt", flt );

    convertScaleAbs( lapl, lapl );
    threshold(lapl, lapl, laplThres, 255, THRESH_BINARY);
    imshow( "lapl", lapl );

    return flt;
}

Mat laplacianop2(Mat src, int times, int thres)
{
    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    GaussianBlur(gray, gray, Size(3, 3), 1);

    Mat kernel(3, 3, CV_32F, Scalar(0));

    kernel.at<float>(1,1)= 5.0;
    kernel.at<float>(0,1)= -1.0;
    kernel.at<float>(2,1)= -1.0;
    kernel.at<float>(1,0)= -1.0;
    kernel.at<float>(1,2)= -1.0;

    // filter the image
    Mat flt;
    filter2D(gray, flt, gray.depth(), kernel);

    for (int i = 0; i < times; i++)
        filter2D(flt, flt, gray.depth(), kernel);

    threshold(flt, flt, thres, 255, CV_THRESH_BINARY);

    imshow( "flt", flt );

    return flt;
}

Mat sobelop(Mat src, int ksize, int bKsize, int thres)
{
    if (ksize % 2 == 0)
        ksize++;

    if (bKsize % 2 == 0)
        bKsize++;

    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    //medianBlur(gray, gray, bKsize);
    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2.0, 2.0);

    /// Generate grad_x and grad_y
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    Mat flt;

    /// Gradient X
    Scharr(gray, grad_x, CV_16S, 1, 0, ksize);
    //Sobel(gray, grad_x, CV_16S, 1, 0, ksize);
    convertScaleAbs(grad_x, abs_grad_x);

    /// Gradient Y
    //Scharr(gray, grad_y, CV_16S, 0, 1, ksize);
    Sobel(gray, grad_y, CV_16S, 0, 1, ksize);
    convertScaleAbs(grad_y / 2, abs_grad_y);

    namedWindow("abs_grad_y", WINDOW_AUTOSIZE);
    imshow("abs_grad_y", abs_grad_y);

    /// Total Gradient (approximate)
    Mat sob;
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, sob);
    namedWindow("sobel", WINDOW_AUTOSIZE);
    imshow("sobel", sob);

    //equalizeHist(sob, sob);

    //threshold(sob, flt, thres, 255, CV_THRESH_BINARY);
    threshold(abs_grad_y, flt, thres, 255, CV_THRESH_BINARY);

    namedWindow("src", WINDOW_AUTOSIZE);
    imshow("src", src);

    imshow("flt", flt);

    return flt;
}

Mat cannyop(Mat src, int thres1, int thres2, int bKsize)
{
    if (bKsize % 2 == 0)
        bKsize++;

    Mat gray, flt;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    //equalizeHist(gray, gray);

    //medianBlur(gray, gray, bKsize);
    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2, 2);

    Canny( gray, flt, thres1, thres2, 3);

    imshow( "flt", flt );

    return flt;
}

Mat kmeansop(Mat src, int k, int attempts, int colorScale, int bKsize)
{
    if (bKsize % 2 == 0)
        bKsize++;

    Mat gray, flt;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    GaussianBlur(gray, gray, Size(bKsize, bKsize), 0.0);

    int nr = src.rows;
    int nc = src.cols;
    int n = nr * nc;
    float scale = (float ) colorScale;

    Mat reshaped = Mat::zeros(n, 3, CV_32F);

    uchar *grow = gray.ptr(0);

    for (int i = 0; i < n; i++)
    {
        reshaped.at<float>(i, 0) = (float ) (i / nc) / (float ) nr;
        reshaped.at<float>(i, 1) = (float ) (i % nc) / (float ) nc;
        reshaped.at<float>(i, 2) = (float ) grow[i] / scale;
    }

    cout << endl;
    cout << "reshaped" << endl;
    cout << endl;

    int l = 0;

    for (int i = 0; i < gray.rows; i++)
    {
        for (int j = 0; j < gray.cols; j++)
        {
            cout << " " << reshaped.at<float>(l, 2);
            l++;
        }

        cout << endl;
    }

    cout << endl;

    Mat bestLabels, centers;

    double compactness = kmeans(reshaped, k, bestLabels, TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, attempts, 1.0), attempts, KMEANS_PP_CENTERS, centers);

    cout << "compactness = " << compactness << endl;

    assert(bestLabels.type() == CV_32SC1);

    //Mat l1 = bestLabels.reshape(1, gray.rows);

    //l1.convertTo(flt, CV_8UC1, 255.0 / (double ) k);

    cout << endl;
    cout << "labels" << endl;
    cout << endl;

    l = 0;

    flt = Mat(nr, nc, CV_8UC1);

    for (int i = 0; i < gray.rows; i++)
    {
        for (int j = 0; j < gray.cols; j++)
        {
            int label = bestLabels.at<int>(l, 0);
            flt.at<uchar>(i, j) = (uchar ) (centers.at<float>(label, 2) * scale);
            cout << " " << (int ) flt.at<uchar>(i, j);
            l++;
        }

        cout << endl;
    }

    cout << endl;

    for (int kk = 0; kk < k; kk++)
    {
        int x = (int ) (centers.at<float>(kk, 1) * (float ) nc);
        int y = (int ) (centers.at<float>(kk, 0) * (float ) nr);
        int c = (uchar ) (centers.at<float>(kk, 2) * scale);
        cout << "center " << kk << "\tx = " << x << "\ty = " << y << "\tcolor = " << c << endl;
    }

    namedWindow( "kmeans", WINDOW_AUTOSIZE );
    imshow("kmeans", flt);

    return flt;
}

Mat bitshift(Mat src, int shift)
{
    if (shift < 0)
        shift = 0;
    else if (shift > 15)
        shift = 15;

    resize(src, src, Size(512, 512));

    ushort max0 = 0;
    int mi, mj;
    for (int i = 0; i < src.rows; i++)
    {
        for (int j = 0; j < src.cols; j++)
        {
            if (max0 < src.at<ushort>(i, j))
            {
                max0 = src.at<ushort>(i, j);
                mi = i;
                mj = j;
            }
        }
    }

    Mat flt = cv::Mat::zeros(src.size(), CV_8U);
    Mat tmp = src.clone();
    double t = (double)getTickCount();

    //for (int i = 0; i < 1000; i++)
    {
        // ------------------------------ MODE 1 ------------------------------ //
        //tmp.convertTo(flt, CV_8U, 1.0 / (double ) (1 << shift));

        // ------------------------------ MODE 2 ------------------------------ //
        //ushort *srow = tmp.ptr<ushort>(0);
        //uchar *frow = flt.ptr<uchar>(0);

        //for (int i = 0; i < src.cols * src.rows; i++)
        //{
        //    if (srow[i] >= (1 << shift) && srow[i] < ((1 << shift) << 8))
        //        frow[i] = (srow[i] >> shift);
        //}

        /*ushort mask = (1 << (shift + 8)) - 1;

        tmp = min(tmp, mask + 1);
        tmp &= mask;
        tmp.convertTo(flt, CV_8U, 1.0 / (double ) (1 << shift));*/

        // ------------------------------ MODE 3 ------------------------------ //
        ushort mask = ((1 << 8) - 1) << shift;

        //ushort *srow = tmp.ptr<ushort>(0);
        //uchar *frow = flt.ptr<uchar>(0);
        //for (int i = 0; i < src.cols * src.rows; i++)
        //    frow[i] = (uchar ) ((srow[i] & mask) >> shift);

        tmp &= mask;
        tmp.convertTo(flt, CV_8U, 1.0 / (double ) (1 << shift));
    }

    t = ((double)getTickCount() - t)/getTickFrequency();

    cout << endl;
    cout << "total time   = " << t << "s" << endl;
    cout << "single time  = " << t / 1000 << "s" << endl;
    cout << "src.channels = " << src.channels() << endl;
    cout << "src.depth    = " << src.depth() << endl;
    cout << "flt.channels = " << flt.channels() << endl;
    cout << "flt.depth    = " << flt.depth() << endl;
    cout << "max0 = " << max0 << endl;
    cout << endl;
    cout << "1.0 / " << (1 << shift) << " = " << (1.0 / (double ) (1 << shift)) << endl;

    cout << "[" << mi << ", " << mj << "] = " << (int ) src.at<ushort>(mi, mj) << endl;
    cout << "[" << mi << ", " << mj << "] = " << (ushort ) (src.at<ushort>(mi, mj) << (16 - (shift + 8))) << endl;
    cout << "[" << mi << ", " << mj << "] = " << (ushort ) ((ushort ) (src.at<ushort>(mi, mj) << (16 - (shift + 8))) >> 8) << endl;

    cout << "[" << mi << ", " << mj << "] = " << (int ) flt.at<uchar>(mi, mj) << endl;

    imshow("flt", flt);

    /*vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(0);
    imwrite("flt.png", src, compression_params);*/

    return flt;
}

Mat myfilter(Mat src, int ksize, int bKsize, int thresh)
{
    if (ksize < 3)
        ksize = 3;

    if (ksize % 2 == 0)
        ksize++;

    if (bKsize % 2 == 0)
        bKsize++;

    int ksize2 = ksize / 2;

    Mat gray;

    cvtColor(src, gray, COLOR_RGB2GRAY);

    medianBlur(gray, gray, bKsize);

    Mat flt = gray.clone();
    Mat w;

    for (int i = ksize2; i < gray.size().height - ksize2; i++)
    {
        for (int j = ksize2; j < gray.size().width - ksize2; j++)
        {
            w = gray.colRange(j - ksize2, j + ksize2).rowRange(i - ksize2, i + ksize2);

            uchar c = w.at<uchar>(ksize2 + 1, ksize2 + 1);
            double km = 255.0;
            uchar kme = c;
            bool done = false;

            for (int ii = 0; ii < ksize && !done; ii++)
            {
                for (int jj = 0; jj < ksize; jj++)
                {
                    if (ii == ksize2 + 1 && ii == jj)
                        continue;

                    uchar e = w.at<uchar>(ii,jj);

                    if (c == e)
                    {
                        //continue;
                        kme = e;
                        done = true;
                        break;
                    }

                    if (c > e && (double ) (c - e) < km)
                    {
                        km = c - e;
                        kme = e;
                    }

                    if (c < e && (double ) (e - c) < km)
                    {
                        km = (double ) (e - c);
                        kme = e;
                    }
                }
            }

            flt.at<uchar>(i,j) = kme;
        }
    }

    Mat d1 = 125 + gray - flt;
    namedWindow( "d1", WINDOW_AUTOSIZE );
    imshow("d1", d1);

    //adaptiveThreshold(d, d1, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 5, 0.0);
    threshold(flt, flt, thresh, 255, CV_THRESH_BINARY);

    imshow( "flt", flt );

    return flt;
}

Mat myfilter2(Mat src, int ksize/*, int thres*/, int bKsize, int band)
{
    if (ksize % 2 == 0)
        ksize++;

    if (bKsize % 2 == 0)
        bKsize++;

    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);
    medianBlur(gray, gray, bKsize);
    //blur(gray, gray, Size(bKsize, bKsize));

    Mat flt = gray.clone();

    Mat b;
    blur(gray, b, Size(ksize, ksize), Point(0, 0));

    Mat er;
    erode(gray, er, Mat(), Point(0,0));

    Mat dil;
    dilate(gray, dil, Mat(), Point(0,0));

    for (int i = 0; i < gray.size().height; i++)
    {
        for (int j = 0; j < gray.size().width ; j++)
        {
            /*
            if (gray.at<uchar>(i,j) > b.at<uchar>(i,j) + band)
                flt.at<uchar>(i,j) = er.at<uchar>(i,j);
            else if (gray.at<uchar>(i,j) < b.at<uchar>(i,j) - band)
                flt.at<uchar>(i,j) = dil.at<uchar>(i,j);
            else
                flt.at<uchar>(i,j) = 0;//b.at<uchar>(i,j);*/

            /*if (gray.at<uchar>(i,j) > b.at<uchar>(i,j) + band)
                flt.at<uchar>(i,j) = dil.at<uchar>(i,j);
            else if (gray.at<uchar>(i,j) < b.at<uchar>(i,j) - band)
                flt.at<uchar>(i,j) = er.at<uchar>(i,j);
            else
                flt.at<uchar>(i,j) = b.at<uchar>(i,j);*/

            /*if (dil.at<uchar>(i,j) - er.at<uchar>(i,j) > band)
                flt.at<uchar>(i,j) = 255;
            else
                flt.at<uchar>(i,j) = 0;*/

            if (abs(b.at<uchar>(i,j) - gray.at<uchar>(i,j)) > band)
                flt.at<uchar>(i,j) = 255;
            else
                flt.at<uchar>(i,j) = 0;
        }
    }

    //flt = gray - flt;
    //equalizeHist(flt, flt);

    //threshold(flt, flt, thres, 255, THRESH_BINARY);


    //dilate(flt, flt, Mat(), Point(0,0), 2);
    //erode(flt, flt, Mat(), Point(0,0), 2);

    /*erode(flt, flt, Mat());
    dilate(flt, flt, Mat());
    */

    imshow( "flt", flt );

    return flt;
}

Mat dirdilate(Mat src, int d)
{
    //Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat flt = src.clone();

    for (int i = d; i < src.rows - d - 1; i++)
    {
        for (int j = d; j < src.cols - d - 1 ; j++)
        {
            //uchar ul = src.at<uchar>(i - d, j - d);
            uchar uc = src.at<uchar>(i - d, j    );
            //uchar ur = src.at<uchar>(i - d, j + d);

            uchar cl = src.at<uchar>(i    , j - d);
            //uchar cc = src.at<uchar>(i    , j    );
            uchar cr = src.at<uchar>(i    , j + d);

            //uchar dl = src.at<uchar>(i + d, j - d);
            uchar dc = src.at<uchar>(i + d, j    );
            //uchar dr = src.at<uchar>(i + d, j + d);

            //if ( ( (ul > 0 || uc > 0 || ur > 0) && (dl > 0 || dc > 0 || dr > 0) ) )
            if ( uc > 0 && dc > 0 )
            {
                flt.at<uchar>(i - d, j    ) = 255;
                flt.at<uchar>(i    , j    ) = 255;
                flt.at<uchar>(i + d, j    ) = 255;
            }

            //if ( ( (ul > 0 || cl > 0 || dl > 0) && (ur > 0 || cr > 0 || dr > 0) ) )
            if ( cl > 0 && cr > 0 )
            {
                flt.at<uchar>(i    , j - d) = 255;
                flt.at<uchar>(i    , j    ) = 255;
                flt.at<uchar>(i    , j + d) = 255;
            }

            /*else {
               flt.at<uchar>(i - 1, j - 1) = 0;
               flt.at<uchar>(i    , j - 1) = 0;
               flt.at<uchar>(i + 1, j - 1) = 0;
               flt.at<uchar>(i - 1, j    ) = 0;
               flt.at<uchar>(i    , j    ) = 0;
               flt.at<uchar>(i + 1, j    ) = 0;
               flt.at<uchar>(i - 1, j + 1) = 0;
               flt.at<uchar>(i    , j + 1) = 0;
               flt.at<uchar>(i + 1, j + 1) = 0;
           }*/
        }
    }

    return flt;
}

Mat myfilter3(Mat src, int dist, int diff, int thres, int bKsize)
{
    Mat_<uchar> gray;
    Mat_<uchar> hloc = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat_<uchar> vloc = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat_<uchar> hflt = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat_<uchar> vflt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (diff < 1)
        return hflt;

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, gray, bKsize);

    imshow( "gray", gray );

    //equalizeHist(gray, gray);

    for (int i = dist; i < gray.size().height - dist; i++)
    {
        for (int j = dist; j < gray.size().width - dist ; j++)
        {
            int p          = gray(i, j);

            int localLeft  = gray(i       , j -    1);
            int localRight = gray(i       , j +    1);
            int farLeft    = gray(i       , j - dist);
            int farRight   = gray(i       , j + dist);

            int localLeftDiff  = abs(p - localLeft );
            int localRightDiff = abs(p - localRight);
            int farLeftDiff    = abs(p - farLeft   );
            int farRightDiff   = abs(p - farRight  );

            if (farLeftDiff > diff || farRightDiff > diff)
            {
                hflt(i, j) = farLeftDiff  + farRightDiff;
                hloc(i, j) = max(localLeftDiff, localRightDiff);
            }

            for (int k = dist; k > 0; k--)
            {
                // horizontal
                if (hloc(i, j - k) > hloc(i, j)) {
                    hflt(i, j) = 0;
                    break;
                } else if (hloc(i, j - k) < hloc(i, j)) {
                    hflt(i, j - k) = 0;
                } else if (hflt(i, j - k) > hflt(i, j)) {
                    hflt(i, j) = 0;
                    break;
                } else if (hflt(i, j - k) < hflt(i, j)) {
                    hflt(i, j - k) = 0;
                } else {
                    int lus = hflt(i - 1, j - 1) + hflt(i - 1, j) + hflt(i - 1, j + 1);
                    int fus = hflt(i - 1, j - k - 1) + hflt(i - 1, j - k) + hflt(i - 1, j - k + 1);

                    //int lus = hflt(i - 1, j    );
                    //int fus = hflt(i - 1, j - k);

                    if (fus > lus)
                    {
                        hflt(i, j) = 0;
                        break;
                    } else {
                        hflt(i, j - k) = 0;
                    }
                }
            }
        }
    }

    for (int j = dist; j < gray.size().width - dist ; j++)
    {
        for (int i = dist; i < gray.size().height - dist; i++)
        {
            int p          = gray(i, j);

            int localUp    = gray(i - 1   , j       );
            int localDown  = gray(i + 1   , j       );
            int farUp      = gray(i - dist, j       );
            int farDown    = gray(i + dist, j       );

            int localUpDiff    = abs(p - localUp   );
            int localDownDiff  = abs(p - localDown );
            int farUpDiff      = abs(p - farUp     );
            int farDownDiff    = abs(p - farDown   );

            if (farUpDiff > diff || farDownDiff > diff)
            {
                vflt(i, j) = farUpDiff  + farDownDiff;
                vloc(i, j) = max(localUpDiff, localDownDiff);
            }

            for (int k = dist; k > 0; k--)
            {
                // vertical
                if (vloc(i - k, j) > vloc(i, j)) {
                    vflt(i, j) = 0;
                    break;
                } else if (vloc(i - k, j) < vloc(i, j)) {
                    vflt(i - k, j) = 0;
                } else if (vflt(i - k, j) > vflt(i, j)) {
                    vflt(i, j) = 0;
                    break;
                } else if (vflt(i - k, j) < vflt(i, j)) {
                    vflt(i - k, j) = 0;
                } else {
                    int lls = vflt(i - 1, j - 1) + vflt(i, j - 1) + vflt(i + 1, j - 1);
                    int fls = vflt(i - k - 1, j - 1) + vflt(i - k, j - 1) + vflt(i - k + 1, j - 1);

                    //int lls = vflt(i    , j - 1);
                    //int fls = vflt(i - k, j - 1);

                    if (fls > lls)
                    {
                        vflt(i, j) = 0;
                        break;
                    } else {
                        vflt(i - k, j) = 0;
                    }
                }
            }
        }
    }

    threshold(hflt, hflt, thres, 255, CV_THRESH_TOZERO);
    threshold(vflt, vflt, thres, 255, CV_THRESH_TOZERO);

    Mat flt = hflt + vflt;

    namedWindow( "hvflt", WINDOW_AUTOSIZE );
    namedWindow( "hflt", WINDOW_AUTOSIZE );
    namedWindow( "vflt", WINDOW_AUTOSIZE );

    imshow( "hvflt", flt );

    imshow( "hflt", hflt );
    imshow( "vflt", vflt );

    threshold(flt, flt, thres, 255, CV_THRESH_BINARY);

    imshow( "flt", flt );

    return flt;
}

Mat myfilter4(Mat src, int dist, int diff, int thres, int bKsize)
{
    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (diff < 1 || dist < 1)
        return gray;

    Mat flt = gray.clone();

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, gray, bKsize);

    int v = 0;
    float m = 0.0;
    float a = (float ) 0.5;

    for (int i = dist - 1; i < gray.size().height - dist; i++)
    {
        v = gray.at<uchar>(i, 0);
        m = (float ) v;

        for (int j = 1; j < dist ; j++)
            m = m * a + (1.0f - a) * (float ) gray.at<uchar>(i, j);

        for (int j = dist; j < gray.size().width - dist ; j++)
        {
            float n = (float ) gray.at<uchar>(i, j);

            if (fabs(n - m) <= diff)
            {
                m = m * a + (1.0f - a) * n;
                flt.at<uchar>(i, j) = v;
            }
            else
            {
                float m1 = gray.at<uchar>(i, j + 1);

                for (int k = 1; k < dist ; k++)
                    m1 = m1 * a + (1.0f - a) * (float ) gray.at<uchar>(i, j + k);

                if (fabs(m1 - m) <= dist)
                    flt.at<uchar>(i, j) = v;
                else
                {
                    flt.at<uchar>(i, j) = (uchar ) n;
                    v = (int ) n;
                    m = n;
                }
            }
        }
    }

    threshold(flt, flt, thres, 255, THRESH_BINARY);

    imshow( "flt", flt );

    return flt;
}

Mat myfilter5(Mat src, int neighborhood, int diff, int bKsize)
{
    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, gray, bKsize);

    if (diff < 1 || neighborhood < 1)
    {
        threshold(gray, gray, 125, 255, THRESH_BINARY);
        return gray;
    }

    int leni = 2 * neighborhood + 1;
    float lenf = 2.0f * (float ) neighborhood + 1.0f;

    Mat flt = cv::Mat::zeros(gray.size(), CV_8UC1);

    float strip[255];

    for (int i = 0; i < gray.size().height; i++)
    {
        int next = 0;
        float acc0 = 0.0f;
        float acc1 = 0.0f;
        int mini = 0;
        int maxi = 0;
        float min = 256.0f;
        float max = 0.0f;

        for (int j = 0; j < leni; j++)
        {
            strip[j] = (float ) gray.at<uchar>(i, j);
            acc0 += strip[j];

            if (strip[j] <= min)
            {
                min = strip[j];
                mini = j;
            }

            if (strip[j] >= max)
            {
                max = strip[j];
                maxi = j;
            }
        }

        acc1 = acc0;

        for (int j = neighborhood; j < gray.size().width - neighborhood; j++)
        {
            bool b = false;

            float last = strip[next];
            acc0 -= last;

            strip[next] = (float ) gray.at<uchar>(i, j + neighborhood);
            acc0 += strip[next];

            if (mini == next || maxi == next)
            {
                min = 256.0;
                max = 0.0;

                for (int k = 0; k < leni; k++)
                {
                    if (strip[k] <= min)
                    {
                        min = strip[k];
                        mini = k;
                    }

                    if (strip[k] >= max)
                    {
                        max = strip[k];
                        maxi = k;
                    }
                }
            }
            else
            {
                if (strip[next] <= min)
                {
                    min = strip[next];
                    mini = next;
                }

                if (strip[next] >= max)
                {
                    max = strip[next];
                    maxi = next;
                }
            }

            if (max - min <= diff)
            {
                if (fabs(acc1 / lenf - acc0 / lenf) >= diff)
                {
                    b = !b;
                    acc1 = acc0;
                }
            }

            flt.at<uchar>(i, j) = b ? 255 : 0;

            next = (next + 1) % leni;
        }
    }

    Mat horiz = flt.clone();
    flt = cv::Mat::zeros(gray.size(), CV_8UC1);

    for (int i = 0; i < gray.size().width; i++)
    {
        int next = 0;
        float acc0 = 0.0;
        float acc1 = 0.0;
        int mini = 0;
        int maxi = 0;
        float min = 256.0;
        float max = 0.0;

        for (int j = 0; j < leni; j++)
        {
            strip[j] = (float ) gray.at<uchar>(j, i);
            acc0 += strip[j];

            if (strip[j] <= min)
            {
                min = strip[j];
                mini = j;
            }

            if (strip[j] >= max)
            {
                max = strip[j];
                maxi = j;
            }
        }

        acc1 = acc0;

        for (int j = neighborhood; j < gray.size().height - neighborhood; j++)
        {
            bool b = false;

            float last = strip[next];
            acc0 -= last;

            strip[next] = (float ) gray.at<uchar>(j + neighborhood, i);
            acc0 += strip[next];

            if (mini == next || maxi == next)
            {
                min = 256.0;
                max = 0.0;

                for (int k = 0; k < leni; k++)
                {
                    if (strip[k] <= min)
                    {
                        min = strip[k];
                        mini = k;
                    }

                    if (strip[k] >= max)
                    {
                        max = strip[k];
                        maxi = k;
                    }
                }
            }
            else
            {
                if (strip[next] <= min)
                {
                    min = strip[next];
                    mini = next;
                }

                if (strip[next] >= max)
                {
                    max = strip[next];
                    maxi = next;
                }
            }

            if (max - min <= diff)
            {
                if (fabs(acc1 / lenf - acc0 / lenf) >= diff)
                {
                    b = !b;
                    acc1 = acc0;
                }
            }

            flt.at<uchar>(j, i) = b ? 255 : 0;

            next = (next + 1) % leni;
        }
    }

    Mat vert = flt.clone();

    flt = horiz + vert;

    imshow( "flt", flt );

    return flt;
}

Mat myfilter6(Mat src, int part, int thres, int bKsize)
{
    Mat gray;
    Mat flt;

    cvtColor(src, gray, COLOR_RGB2GRAY);

    //imshow( "gray", gray );

    if (part < 1)
        return gray;

    if (part % 2 == 1)
        part++;

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, gray, bKsize);

    //equalizeHist(gray, gray);

    flt = gray / part;

    threshold(flt, flt, thres - 1, 255, THRESH_TOZERO);
    threshold(flt, flt, thres + 1, 255, THRESH_TOZERO_INV);

    threshold(flt, flt, thres, 255, THRESH_BINARY);

    imshow( "flt", flt );

    return flt;
}

Mat myfilter7(Mat src, int part, int thres, int bKsize, int thres2)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    if (bKsize % 2 == 0)
        bKsize++;

    cvtColor(src, gray, COLOR_RGB2GRAY);

    medianBlur(gray, gray, bKsize);

    if (part < 1)
        return gray;

    if (thres % 2 == 0)
        thres++;

    //equalizeHist(gray, gray);

    Mat grayc = gray.clone();
    threshold(gray, gray, 255 - thres, 255 - thres, THRESH_TRUNC);

    gray = (gray - thres) / part;

    std::vector<std::vector<Point> > allLevelsContours;

    for (int l = 1; l < (255 - thres) / part; l++)
    {
        Mat band = gray.clone();

        // working a band of the spectrum
        threshold(band, band, l - 1, 255, THRESH_TOZERO);
        threshold(band, band, l + 1, 255, THRESH_TOZERO_INV);

        // easy but not taking a band only
        //threshold(band, band, l, 255, THRESH_BINARY);

        std::vector<std::vector<Point> > contours;

        findContours(band,
                     contours,
                     CV_RETR_LIST,
                     CV_CHAIN_APPROX_SIMPLE);

        Mat level = cv::Mat::zeros(src.size(), CV_8UC1);

        drawContours(level,      // image
                     contours,   // contours
                     -1,         // draw all contours
                     5,          // contours color
                     1);         // thickness

        flt += level;

        allLevelsContours.insert(allLevelsContours.end(), contours.begin(), contours.end());
    }

    drawContours(grayc,        // image
                 allLevelsContours,   // contours
                 -1,         // draw all contours
                 255,        // contours color
                 1);         // thickness

    threshold(flt, flt, thres2, 255, CV_THRESH_BINARY);

    imshow("flt", flt);
    imshow("grayc", grayc);

    return flt;
}

#define SIDE_LENGTH 50.0
#define RADIUS 50.0 / 4.0
#define CIRCLE_AREA RADIUS * RADIUS * M_PI
#define PLATE_BACKGROUND_AREA SIDE_LENGTH * SIDE_LENGTH - CIRCLE_AREA

Mat myfilter8(Mat src, int bKsize)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, gray, bKsize);

    int hist[256];
    int mi = 0;
    int max = 0;

    for (int i = 0; i < 256; i++)
        hist[i] = 0;

    for (int r = 0; r < gray.size().height; r++)
    {
        for (int c = 0; c < gray.size().width; c++)
        {
            int g = gray.at<uchar>(r, c);
            hist[g]++;

            if (hist[g] > max)
            {
                mi = g;
                max = hist[g];
            }
        }
    }

    int lowerThresh = mi;
    int upperThresh = mi;
    bool direction = true;
    int sum = hist[mi];

    int area = (int ) (PLATE_BACKGROUND_AREA);

    while (sum < area)
    {
        if (direction)
        {
            upperThresh++;
            sum += hist[upperThresh];
        }
        else
        {
            lowerThresh--;
            sum += hist[lowerThresh];
        }

        direction = !direction;
    }

    Mat lower;
    Mat upper;
    threshold(gray, lower, lowerThresh, 255, THRESH_BINARY_INV);
    threshold(gray, upper, upperThresh, 255, THRESH_BINARY);

    flt = lower + upper;

    imshow("flt", flt);

    return flt;
}

Mat myfilter9(Mat src, int distance/*, int thres*/, int bKsize)
{
    Mat_<uchar> gray;
    Mat_<uchar> flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    //medianBlur(gray, gray, bKsize);

    int nr = src.rows;
    int nc = src.cols;

    for (int rr = 1; rr < nr; rr++)
    {
        uchar *prow = gray.ptr(rr - 1);
        uchar *crow = gray.ptr(rr);
        uchar *frow = flt.ptr(rr);

        uchar prevh = crow[1];
        uchar prevv = prow[0];
        for (int cc = 1; cc < nc; cc++)
        {
            uchar cur = crow[cc];
            uchar d = 125 + 2*cur - prevh - prevv;
            if (d < 125 - distance || d > 125 + distance)
                frow[cc] = d;

            //uchar d = 125 + abs(cur - prevh) + abs(cur -prevv);
            //if (d > 125 + distance)
            //frow[cc] = d;

            prevh = cur;
            prevv = prow[cc];
        }
    }

    //threshold(flt, flt, thres, 255, CV_THRESH_BINARY);

    imshow("flt", flt);

    return flt;
}

Mat myfilter10(Mat src, int var1, int var2, int thres, int bKsize)
{
    Mat_<uchar> gray;
    Mat_<uchar> flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, gray, bKsize);

    int nr = src.rows;
    int nc = src.cols;

    for (int r = 1; r < nr - 1; r++)
    {
        uchar *usr = gray.ptr(r - 1);
        uchar *csr = gray.ptr(r);
        uchar *dsr = gray.ptr(r + 1);

        uchar *udr = flt.ptr(r - 1);
        uchar *cdr = flt.ptr(r);
        uchar *ddr = flt.ptr(r + 1);

        for (int c = 1; c < nc; c++)
        {
            int ld = 0;
            int rd = 0;

            int ul = usr[c - 1];
            int uc = usr[c    ];
            int ur = usr[c + 1];

            int cl = csr[c - 1];
            int cc = csr[c    ];
            int cr = csr[c + 1];

            int dl = dsr[c - 1];
            int dc = dsr[c    ];
            int dr = dsr[c + 1];

            if (abs(uc - cc) < var1 && abs(cc - dc) < var1)
            {
                ld = abs(cc - (ul + cl + dl) / 3);
                rd = abs(cc - (ur + cr + dr) / 3);

                if (ld < var2 && rd < var2)
                    continue;

                if (ld > rd)
                {
                    udr[c] = cdr[c] = ddr[c] = ld;
                } else {
                    udr[c] = cdr[c] = ddr[c] = rd;
                }
            }
        }
    }

    threshold(flt, flt, thres, 255, CV_THRESH_BINARY);

    imshow("flt", flt);

    return flt;
}

Mat myfilter11(Mat src, int dist, int thres1, int bKsize)
{
    Mat_<uchar> gray;
    Mat_<uchar> flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    Mat_<uchar> grayc = gray.clone();
    //bilateralFilter(grayc, gray, bKsize, 10, 5);
    medianBlur(gray, gray, bKsize);

    int nr = src.rows;
    int nc = src.cols;

    // horizontal
    for (int r = 0; r < nr; r++)
    {
        uchar *srow = gray.ptr(r);
        uchar *frow = flt.ptr(r);

        int start = -1;
        bool dir = true;

        for (int c = dist; c < nc; c++)
        {
            int prec = srow[c - dist];
            int cur = srow[c];

            if (start == -1 && cur > 150)
                continue;

            if (cur < prec - thres1)
            {
                if (start != -1 && dir == false)
                {
                    frow[start + (c - start) / 2] = 255;
                    start = -1;
                }

                if (start == -1)
                {
                    start = c;
                    dir = true;
                }
                continue;
            }

            if (cur > prec + thres1)
            {
                if (start != -1 && dir == true)
                {
                    frow[start + (c - start) / 2] = 255;
                    start = -1;
                }

                if (start == -1)
                {
                    start = c;
                    dir = true;
                }

                continue;
            }

            if (start != -1)
            {
                frow[start + (c - start) / 2] = 255;
                start = -1;
            }
        }
    }

    // vertical
    for (int c = dist; c < nc; c++)
    {
        int start = -1;
        bool dir = true;

        for (int r = 0; r < nr; r++)
        {
            uchar *srow = gray.ptr(r);
            uchar *sprow = gray.ptr(r - dist);

            int prev = sprow[c];
            int cur = srow[c];

            if (start == -1 && cur > 150)
                continue;

            if (cur < prev - thres1)
            {
                if (start != -1 && dir == false)
                {
                    uchar *frow = flt.ptr(start + (r - start) / 2);
                    frow[c] = 255;

                    start = -1;
                }

                if (start == -1)
                {
                    start = r;
                    dir = true;
                }

                continue;
            }

            if (cur > prev + thres1)
            {
                if (start != -1 && dir == true)
                {
                    uchar *frow = flt.ptr(start + (r - start) / 2);
                    frow[c] = 255;

                    start = -1;
                }

                if (start == -1)
                {
                    start = r;
                    dir = false;
                }

                continue;
            }

            if (start != -1)
            {
                uchar *frow = flt.ptr(start + (r - start) / 2);
                frow[c] = 255;

                start = -1;
            }
        }
    }


    threshold(flt, flt, 1, 255, CV_THRESH_BINARY);

    namedWindow( "gray", WINDOW_AUTOSIZE );
    imshow("gray", gray);

    imshow("flt", flt);

    return flt;
}

Mat myfilter12(Mat src, int thres1, int thres2, int check, int bKsize)
{
    Mat_<uchar> gray;
    Mat_<uchar> flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (!gray.isContinuous())
        return gray;

    if (bKsize % 2 == 0)
        bKsize++;

    Mat_<uchar> grayc = gray.clone();
    bilateralFilter(grayc, gray, bKsize, 20, 5);
    //medianBlur(gray, gray, bKsize);

    int nr = src.rows;
    int nc = src.cols;

    uchar *srow = gray.ptr(0);
    uchar *frow = flt.ptr(0);

    int i = 0;

    // horizontal scan
    for (int r = 0; r < nr; r++)
    {
        int start = -1;
        int startValue;
        bool dir = true;

        int prec = 0;
        int cur = 0;
        int checkc = 0;
        int checkIndex = -1;
        int checkDiff;

        for (int c = 0; c < nc; c++)
        {
            prec = cur;
            cur = srow[i];
            i++;

            if (start == -1 && cur > 150)
                continue;

            if (checkc > 0)
                checkc--;

            if (cur > prec + thres1)
            {
                if (start != -1 && dir == false)
                {
                    int diff = startValue - prec;

                    if (diff > thres2)
                    {
                        if (checkc == 0 || (checkc > 0 && diff > checkDiff))
                        {
                            if (checkc > 0)
                                frow[checkIndex] = 0;

                            checkIndex = start + (i - 1 - start) / 2;
                            frow[checkIndex] = 255;
                            checkc = check;
                            checkDiff = diff;
                        }
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    start = i - 1;
                    startValue = prec;
                    dir = true;
                }
                continue;
            }

            if (cur < prec - thres1)
            {
                if (start != -1 && dir == true)
                {
                    int diff = prec - startValue;

                    if (diff > thres2)
                    {
                        if (checkc == 0 || (checkc > 0 && diff > checkDiff))
                        {
                            if (checkc > 0)
                                frow[checkIndex] = 0;

                            checkIndex = start + (i - 1 - start) / 2;
                            frow[checkIndex] = 255;
                            checkc = check;
                            checkDiff = diff;
                        }
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    start = i - 1;
                    startValue = prec;
                    dir = false;
                }

                continue;
            }

            if (start != -1)
            {
                int diff = abs(startValue - prec);

                if (diff > thres2)
                {
                    if (checkc == 0 || (checkc > 0 && diff > checkDiff))
                    {
                        if (checkc > 0)
                            frow[checkIndex] = 0;

                        checkIndex = start + (i - 1 - start) / 2;
                        frow[checkIndex] = 255;
                        checkc = check;
                        checkDiff = diff;
                    }
                }

                start = -1;
            }
        }
    }

    // vertical scan
    for (int c = 0; c < nc; c++)
    {
        int start = -1;
        int startValue;
        bool dir = true;

        int prec = 0;
        int cur = 0;
        int checkc = 0;
        int checkIndex = -1;
        int checkDiff;

        i = c;

        for (int r = 0; r < nr; r++)
        {
            prec = cur;
            cur = srow[i];
            i += nc;

            if (start == -1 && cur > 150)
                continue;

            if (checkc > 0)
                checkc--;

            if (cur > prec + thres1)
            {
                if (start != -1 && dir == false)
                {
                    int diff = startValue - prec;

                    if (diff > thres2)
                    {
                        if (checkc == 0 || (checkc > 0 && diff > checkDiff))
                        {
                            if (checkc > 0)
                                frow[checkIndex] = 0;

                            checkIndex = (start + (r - 1 - start) / 2) * nc + c;
                            frow[checkIndex] = 255;
                            checkc = check;
                            checkDiff = diff;
                        }
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    start = r - 1;
                    startValue = prec;
                    dir = true;
                }
                continue;
            }

            if (cur < prec - thres1)
            {
                if (start != -1 && dir == true)
                {
                    int diff = prec - startValue;

                    if (diff > thres2)
                    {
                        if (checkc == 0 || (checkc > 0 && diff > checkDiff))
                        {
                            if (checkc > 0)
                                frow[checkIndex] = 0;

                            checkIndex = (start + (r - 1 - start) / 2) * nc + c;
                            frow[checkIndex] = 255;
                            checkc = check;
                            checkDiff = diff;
                        }
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    start = r - 1;
                    startValue = prec;
                    dir = false;
                }

                continue;
            }

            if (start != -1)
            {
                int diff = abs(startValue - prec);

                if (diff > thres2)
                {
                    if (checkc == 0 || (checkc > 0 && diff > checkDiff))
                    {
                        if (checkc > 0)
                            frow[checkIndex] = 0;

                        checkIndex = (start  + (r - 1 - start) / 2) * nc + c;
                        frow[checkIndex] = 255;
                        checkc = check;
                        checkDiff = diff;
                    }
                }

                start = -1;
            }
        }
    }

    //threshold(flt, flt, 1, 255, CV_THRESH_BINARY);

    namedWindow( "gray", WINDOW_AUTOSIZE );
    imshow("gray", gray);

    imshow("flt", flt);

    return flt;
}

Mat myfilter13(Mat src, int dist, int bKsize)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    medianBlur(gray, gray, bKsize);

    int min = 255;
    int max = 0;
    int mean = 0;

    int nr = src.rows;
    int nc = src.cols;

    uchar *srow = gray.ptr(0);
    uchar *frow = flt.ptr(0);

    for (int i = 0; i < nr * nc; i++)
    {
        int g = srow[i];

        if (g < min)
            min = g;

        if (g > max)
            max = g;

        mean += g;
    }

    mean /= nr * nc;

    for (int i = 0; i < nr * nc; i++)
    {
        int g = srow[i];

        //if (g - min < abs(mean - g))
        if (g - min - dist < max - g)
            frow[i] = 255;
        else
            frow[i] = 0;
    }

    imshow("flt", flt);

    return flt;
}

Mat myfilter12s(Mat src, int thres1, int thres2, int thres3, int bKsize)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    //medianBlur(gray, gray, bKsize);
    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2, 2);

    if (!src.isContinuous())
        return gray;

    try
    {
        int nr = src.rows;
        int nc = src.cols;

        uchar *srow = gray.ptr(0);
        uchar *frow = flt.ptr(0);

        int i = 0;

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            int start = -1;
            int startValue;
            bool dir = true;

            int prec = 0;
            int cur = 0;

            i = c;

            for (int r = 0; r < nr; r++)
            {
                prec = cur;
                cur = srow[i];

                i += nc;

                if (start == -1 && cur > 150)
                    continue;

                if (cur > prec + thres1)
                {
                    if (start != -1 && dir == false)
                    {
                        int diff = startValue - prec;

                        if (diff > thres3)
                            frow[i - nc] = 255;
                        else if (diff > thres2 && c > 0) {
                            if (frow[i - nc - 1] == 255)
                                frow[i - nc] = 255;
                        }

                        start = -1;
                    }

                    if (start == -1)
                    {
                        start = r - 1;
                        startValue = prec;
                        dir = true;
                    }
                    continue;
                }

                if (cur < prec - thres1)
                {
                    if (start != -1 && dir == true)
                    {
                        int diff = prec - startValue;

                        if (diff > thres3)
                            frow[i - nc] = 255;
                        else if (diff > thres2 && c > 0) {
                            if (frow[i - nc - 1] == 255)
                                frow[i - nc] = 255;
                        }

                        start = -1;
                    }

                    if (start == -1)
                    {
                        start = r - 1;
                        startValue = prec;
                        dir = false;
                    }

                    continue;
                }

                if (start != -1)
                {
                    int diff = abs(startValue - prec);

                    if (diff > thres3)
                        frow[i - nc] = 255;
                    else if (diff > thres2 && c > 0) {
                        if (frow[i - nc - 1] == 255)
                            frow[i - nc] = 255;
                    }

                    start = -1;
                }
            }
        }

        src = flt;
    } catch( Exception &e ) {

        throw e;
    }

    imshow("flt", flt);

    return flt;
}

#define DEFAULT_MAX_GRAY_LEVEL 192
#define DEFAULT_GRID_ROWS 8
#define DEFAULT_GRID_COLS 16

Mat myfilter14(Mat image, int thres1, int thres2, int len, int perc)
{
    Mat gray;
    cvtColor( image, gray, COLOR_RGB2GRAY );

    try
    {
        if (!image.isContinuous())
            return gray;

        //GaussianBlur(gray, gray, Size(3, 3), 0.0);
        //Mat grayc = gray.clone();
        //bilateralFilter(grayc, gray, 3, 8, 5);

        std::vector<std::vector<std::vector<uchar> > > histograms;
        std::vector<std::vector<uchar> > thresholds;

        int nr = image.rows;
        int nc = image.cols;

        Mat diffs = cv::Mat::zeros(image.size(), CV_16SC1);
        Mat flt = cv::Mat::zeros(image.size(), CV_8UC1);

        uchar *drow = gray.ptr(0);
        short *frow = diffs.ptr<short>(0);
        uchar *fltRow = flt.ptr(0);

        int height = image.rows / DEFAULT_GRID_ROWS;
        int width = image.cols / DEFAULT_GRID_COLS;

        int i = 0;

        // print pixel (gray) values
        /*for (int r = 0; r < nr; r++)
        {
            std::cout << std::endl;
            for (int c = 0; c < nc; c++)
            {
                int cur = drow[i];

                if (cur >= 0)
                {
                    if (cur < 10)
                        std::cout << "    " << cur;
                    else if (cur < 100)
                        std::cout << "   " << cur;
                    else
                        std::cout << "  " << cur;
                } else {
                    if (cur > -10)
                        std::cout << "   " << cur;
                    else if (cur > -100)
                        std::cout << "  " << cur;
                    else
                        std::cout << " " << cur;
                }

                i++;
            }
        }

        std::cout << std::endl;
        std::cout << std::endl;*/

        for (int r = 0; r < DEFAULT_GRID_ROWS; r++)
        {
            histograms.push_back(std::vector<std::vector<uchar> >());
            thresholds.push_back(std::vector<uchar>());

            for (int c = 0; c < DEFAULT_GRID_COLS; c++)
            {
                histograms[r].push_back(std::vector<uchar>());

                for (int h = 0; h < 256; h++)
                    histograms[r][c].push_back(0);
            }
        }

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            i = c;

            int start = -1;
            int startValue;
            bool dir = true;

            int maxi = 0;
            int maxDiff = 0;

            int prec = 0;
            int cur = drow[i];

            int hj = c / width;
            if (hj >= DEFAULT_GRID_COLS)
                hj = DEFAULT_GRID_COLS - 1;

            for (int r = 0; r < nr; r++)
            {
                int hi = r / height;
                if (hi >= DEFAULT_GRID_ROWS)
                    hi = DEFAULT_GRID_ROWS - 1;

                prec = cur;
                cur = drow[i];

                i += nc;

                histograms[hi][hj][cur]++;

                if (start == -1 && (cur > DEFAULT_MAX_GRAY_LEVEL || prec > DEFAULT_MAX_GRAY_LEVEL))
                    continue;

                if (cur > DEFAULT_MAX_GRAY_LEVEL)
                {
                    int diff = abs(startValue - prec);

                    if (diff > thres2)
                        frow[maxi] = dir ? diff : -diff;

                    start = -1;
                    continue;
                }

                if (cur > prec + thres1)
                {
                    if (start != -1 && dir == false)
                    {
                        int diff = startValue - prec;

                        if (diff > thres2)
                            frow[maxi] = -diff;

                        start = -1;
                    }

                    if (start == -1 && cur < DEFAULT_MAX_GRAY_LEVEL && prec < DEFAULT_MAX_GRAY_LEVEL)
                    {
                        start = r - 1;
                        startValue = prec;
                        dir = true;
                        maxDiff = 0;
                    }
                } else if (cur < prec - thres1) {
                    if (start != -1 && dir == true)
                    {
                        int diff = prec - startValue;

                        if (diff > thres2)
                            frow[maxi] = +diff;

                        start = -1;
                    }

                    if (start == -1 && cur < DEFAULT_MAX_GRAY_LEVEL && prec < DEFAULT_MAX_GRAY_LEVEL)
                    {
                        start = r - 1;
                        startValue = prec;
                        dir = false;
                        maxDiff = 0;
                    }
                } else if (start != -1) {
                    int diff = abs(startValue - prec);

                    if (diff > thres2)
                        frow[maxi] = dir ? diff : -diff;

                    start = -1;
                    maxDiff = 0;
                }

                if (start != -1 && abs(cur - prec) >= maxDiff)
                {
                    maxi = i - nc;
                    maxDiff = abs(cur - prec);
                }
            }
        }

        for (int hi = 0; hi < DEFAULT_GRID_ROWS; hi++)
        {
            int h = height;
            if (hi == DEFAULT_GRID_ROWS - 1)
                 h += nr % DEFAULT_GRID_ROWS;

            for (int hj = 0; hj < DEFAULT_GRID_COLS; hj++)
            {
                int w = width;
                if (hj == DEFAULT_GRID_COLS - 1)
                    w += nc % DEFAULT_GRID_COLS;

                int whiteCount = 0;
                for (i = DEFAULT_MAX_GRAY_LEVEL; i < 256; i++)
                    whiteCount += histograms[hi][hj][i];

                double area = width * height - whiteCount;

                int lowLimit = (int ) (area * 0.05);
                int halfArea = (int ) (area / 2.0);
                int sum = 0;
                int low = 0;

                for (i = 0; i < DEFAULT_MAX_GRAY_LEVEL && sum < halfArea; i++)
                {
                    sum += histograms[hi][hj][i];

                    if (sum < lowLimit)
                        low = i;
                }

                thresholds[hi].push_back((uchar ) ((i - low) > 25 ? (i - low) : 25));
                std::cout << "threshold[" << hi << "][" << hj << "] = " << (int ) thresholds[hi][hj] << std::endl;
            }
        }

        i = 0;

        for (int r = 0; r < nr; r++)
        {
            int start = -1;
            bool found = false;
            bool dir = false;

            int hi = r / height;
            if (hi >= DEFAULT_GRID_ROWS)
                hi = DEFAULT_GRID_ROWS - 1;

            for (int c = 0; c < nc; c++)
            {
                int hj = c / width;
                if (hj >= DEFAULT_GRID_COLS)
                    hj = DEFAULT_GRID_COLS - 1;

                short d = frow[i];

                /*if (d >= 0)
                {
                    if (d < 10)
                        std::cout << "    " << d;
                    else if (d < 100)
                        std::cout << "   " << d;
                    else
                        std::cout << "  " << d;
                } else {
                    if (d > -10)
                        std::cout << "   " << d;
                    else if (d > -100)
                        std::cout << "  " << d;
                    else
                        std::cout << " " << d;
                }*/

                if (d != 0)
                {
                    if ((d > 0 && dir == false) || (d < 0 && dir == true) || (c == nc - 1))
                    {
                        if (found)
                        {
                            if (i - start > 4)
                            {
                                for (int s = start; s < i; s++)
                                {
                                    fltRow[s] = 255;
                                    frow[s] = 0;
                                }
                            } else {
                                for (int s = start; s < i; s++)
                                    fltRow[s] = 255;
                            }
                        }

                        start = -1;
                        found = false;
                    }

                    if (start == -1)
                    {
                        start = i;
                        dir = d > 0;
                    }

                    if (abs(d) > thresholds[hi][hj])
                        found = true;
                    /*else
                        frow[i] = 0;*/
                } else {
                    if (found)
                    {
                        if (i - start > 4)
                        {
                            for (int s = start; s < i; s++)
                            {
                                fltRow[s] = 255;
                                frow[s] = 0;
                            }
                        } else {
                            for (int s = start; s < i; s++)
                                fltRow[s] = 255;
                        }
                    }

                    start = -1;
                    found = false;
                }

                i++;
            }

            //std::cout << std::endl;
        }
        //std::cout << std::endl;

        int r = 0;

        if (len <= 2)
            len = 2;

        height = len;

        while (r < nr)
        {
            int width = 2 * len;
            int c = 0;

            while (c < nc)
            {
                int ix = r * nc + c;

                int count = 0;

                for (int rr = 0; rr < height; rr++)
                {
                    for (int cc = 0; cc < width; cc++)
                    {
                        if (frow[ix + rr * nc + cc] != 0)
                            count++;
                    }
                }

                double p = (double ) count / (double ) (width * height);

                if (p > (double ) perc / 100.0)
                {
                    for (int rr = 0; rr < height; rr++)
                        for (int cc = 0; cc < width; cc++)
                            fltRow[ix + rr * nc + cc] = 0;
                }

                c += width;

                if (c != nc && c + width * 2 > nc)
                    width = nc - c;
            }

            r += height;

            if (r != nr && r + height * 2 > nr)
                height = nr - r;
        }


        imshow("flt", flt);

        return flt;

    } catch( Exception &e ) {

        throw e;
    }
}

Mat myfilter15(Mat src, int morphTypeIndex, int kernelSize, int times)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (!gray.isContinuous())
        return gray;

    GaussianBlur(gray, gray, Size(5, 5), 0.0);

    int morphType;

    switch (morphTypeIndex) {
    case 1:
        morphType = MORPH_CROSS;
        break;
    case 2:
        morphType = MORPH_ELLIPSE;
        break;
    default:
        morphType = MORPH_RECT;
        break;
    }

    Mat element = getStructuringElement(morphType,
                                        Size(2 * kernelSize + 1, 2 * kernelSize + 1),
                                        Point(kernelSize, kernelSize));

    for (int t = 0; t < times; t++)
        medianBlur(gray, gray, 5);

    Mat er;
    erode(gray, er, element, Point(-1, -1), 1);

    Mat dil;
    dilate(gray, dil, element, Point(-1, -1), 1);

    int nr = gray.rows;
    int nc = gray.cols;

    int i = 0;

    uchar *grow = gray.ptr(0);
    uchar *erow = er.ptr(0);
    uchar *drow = dil.ptr(0);
    uchar *frow = flt.ptr(0);

    for (int r = 0; r < nr; r++)
    {
        for (int c = 0; c < nc; c++)
        {
            if (grow[i] - erow[i] > drow[i] - grow[i])
                frow[i] = drow[i];
            else
                frow[i] = erow[i];

            i++;
        }
    }

    imshow("flt", flt);

    return flt;
}

Mat cleanFilter(Mat src)
{
     Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    int nr = src.rows;
    int nc = src.cols;

    uchar *sptr = src.ptr(0);
    uchar *dptr = flt.ptr(0);

    for (int r = 1; r < nr - 1; r++)
    {
        for (int c = 1; c < nc; c++)
        {
            int p = sptr[r * nc + c];
            int p1 = sptr[(r - 1) * nc + c - 1];
            int p2 = sptr[(r - 1) * nc + c];
            int p3 = sptr[(r - 1) * nc + c + 1];
            int p4 = sptr[r * nc + c - 1];
            int p6 = sptr[r * nc + c + 1];
            int p7 = sptr[(r + 1) * nc + c - 1];
            int p8 = sptr[(r + 1) * nc + c];
            int p9 = sptr[(r +- 1) * nc + c + 1];

            int diff = p1 + p2 + p3 + p4  + p6 + p7 + p8 + p9 - p * 8;


            dptr[r * nc + c] = (uchar ) (p + diff / 8);
        }
    }

    return flt;
}

Mat myfilter16(Mat src, int thres, int bKsize)
{
    Mat_<uchar> gaussian;
    Mat_<uchar> adiffs = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat_<uchar> pdiffs = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat_<uchar> apdiffs = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat_<uchar> aapdiffs = cv::Mat::zeros(src.size(), CV_8UC1);
    Mat_<uchar> flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gaussian, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    GaussianBlur(gaussian, gaussian, Size(bKsize, bKsize), 2.0);


    gaussian = cleanFilter(gaussian);

    int nr = src.rows;
    int nc = src.cols;

    uchar *gptr = gaussian.ptr(0);
    uchar *adptr = adiffs.ptr(0);
    uchar *pdptr = pdiffs.ptr(0);
    uchar *apdptr = apdiffs.ptr(0);
    uchar *aapdptr = aapdiffs.ptr(0);

    for (int r = 1; r < nr - 1; r++)
    {
        for (int c = 1; c < nc; c++)
        {
            int p = gptr[r * nc + c];
            int p1 = gptr[(r - 1) * nc + c - 1];
            int p2 = gptr[(r - 1) * nc + c];
            int p3 = gptr[(r - 1) * nc + c + 1];
            int p4 = gptr[r * nc + c - 1];
            int p6 = gptr[r * nc + c + 1];
            int p7 = gptr[(r + 1) * nc + c - 1];
            int p8 = gptr[(r + 1) * nc + c];
            int p9 = gptr[(r +- 1) * nc + c + 1];

            int padiffs = absdiff(p, p1) +
                    absdiff(p, p2) +
                    absdiff(p, p3) +
                    absdiff(p, p4) +
                    absdiff(p, p6) +
                    absdiff(p, p7) +
                    absdiff(p, p8) +
                    absdiff(p, p9);

            int ppdiffs = p1 + p2 + p3 + p4  + p6 + p7 + p8 + p9 - p * 8;

            int papdiffs = abs(ppdiffs);

            int pamapdiffs = papdiffs != padiffs ? papdiffs / (padiffs - papdiffs) : papdiffs;

            padiffs %= 255;
            papdiffs %= 255;

            adptr[r * nc + c] = (uchar ) padiffs;
            pdptr[r * nc + c] = (uchar ) ((int ) p + ppdiffs / 8);
            apdptr[r * nc + c] = (uchar ) papdiffs;
            aapdptr[r * nc + c] = (uchar) (p - pamapdiffs);
        }
    }

    threshold(aapdiffs, flt, thres, 255, CV_THRESH_BINARY);

    namedWindow( "gaussian", WINDOW_AUTOSIZE );
    imshow("gaussian", gaussian);

    namedWindow( "adiffs", WINDOW_AUTOSIZE );
    imshow("adiffs", adiffs);

    namedWindow( "pdiffs", WINDOW_AUTOSIZE );
    imshow("pdiffs", pdiffs);

    namedWindow( "apdiffs", WINDOW_AUTOSIZE );
    imshow("apdiffs", apdiffs);

    namedWindow( "aapdiffs", WINDOW_AUTOSIZE );
    imshow("aapdiffs", aapdiffs);

    imshow("flt", flt);

    return flt;
}

Mat myfilter17(Mat src, int thres, int thres2, int bKsize)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2.0);

    //equalizeHist(gray, gray);

    if (!src.isContinuous())
        return gray;

    //print_values(gray);

    try
    {
        int nr = src.rows;
        int nc = src.cols;

        uchar *srow = gray.ptr<uchar>(0);
        uchar *frow = flt.ptr<uchar>(0);

        int i = 0;

        // dir == true  ===> increasing
        // dir == false ===> decreasing
        bool dir = false;

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            int start = -1;
            int startValue;

            int prec = srow[c];
            int cur = prec;

            i = c;

            for (int r = 0; r < nr; r++)
            {
                prec = cur;
                cur = srow[i];

                i += nc;

                if (cur >= prec)
                {
                    if (dir && start != -1 && (cur - prec < thres2 || r == nr - 1))
                    {
                        int diff = prec - startValue;

                        if (diff > thres)
                        {
                            //frow[start] = startValue;
                            //frow[i - nc - nc] = prec;
                            frow[start] = 128;
                            frow[i - nc - nc] = 255;
                        }

                        start = -1;
                    }

                    if (!dir && start != -1)
                    {
                        // stop decreasing

                        int diff = startValue - prec;

                        if (diff > thres)
                        {
                            //frow[start] = startValue;
                            //frow[i - nc - nc] = prec;
                            frow[start] = 255;
                            frow[i - nc - nc] = 128;
                        }

                        start = -1;
                    }

                    if (start == -1 && cur - prec >= thres2)
                    {
                        start = i - nc - nc;
                        startValue = prec;
                        dir = true;
                    }
                }
                else
                {
                    if (!dir && start != -1 && (prec - cur < thres2 || r == nr - 1))
                    {
                        int diff = startValue - prec;

                        if (diff > thres)
                        {
                            //frow[start] = startValue;
                            //frow[i - nc - nc] = prec;
                            frow[start] = 255;
                            frow[i - nc - nc] = 128;
                        }

                        start = -1;
                    }

                    if (dir && start != -1)
                    {
                        // stop increasing

                        int diff = prec - startValue;

                        if (diff > thres)
                        {
                            //frow[start] = startValue;
                            //frow[i - nc - nc] = prec;
                            frow[start] = 128;
                            frow[i - nc - nc] = 255;
                        }

                        start = -1;
                    }

                    if (start == -1 && prec - cur >= thres2)
                    {
                        start = i - nc - nc;
                        startValue = prec;
                        dir = false;
                    }
                }
            }
        }

    } catch( Exception &e ) {

        cerr << "Exception (line " << e.line << ") : " << e.msg << endl;
        throw e;
    }

    namedWindow( "gaussian", WINDOW_AUTOSIZE );
    imshow("gaussian", gray);

    imshow("flt", flt);

    return flt;
}

#define MYFILTER18_DEFAULT_TTL 2

class Interval
{
public:
    uchar highValue;
    uchar lowValue;

    int firstRow;
    int lastRow;

    bool direction;
    int ttl;

    vector<Point> points;

    Interval();
    bool merge(Interval &interval);
    void draw(Mat &src);
};

Interval::Interval()
{
    ttl = MYFILTER18_DEFAULT_TTL;
}

#define MYFILTER18_MIN_COLOR_OVERLAP 0.5
#define MYFILTER18_MIN_SPACE_OVERLAP 0.45
#define MYFILTER18_MIN_CONTOUR_SIZE 10
#define MYFILTER18_MAX_CONTOUR_SIZE 30

bool Interval::merge(Interval &interval)
{
    // check direction
    if (this->direction != interval.direction)
        return false;

    // check space overlap
    if (this->firstRow > interval.lastRow || this->lastRow < interval.firstRow)
        return false;

    // check color overlap
    if (this->lowValue > interval.highValue || this->highValue < interval.lowValue)
        return false;

    // calc space overlap
    if (this->firstRow <= interval.firstRow)
    {
        if (this->lastRow < interval.lastRow)
        {
            double overlap = (double ) (this->lastRow - interval.firstRow) / (double ) (this->lastRow - this->firstRow);

            if (!(overlap > MYFILTER18_MIN_SPACE_OVERLAP))
                return false;
        }
    } else {
        if (this->lastRow > interval.lastRow)
        {
            double overlap = (double ) (interval.lastRow - this->firstRow) / (double ) (this->lastRow - this->firstRow);

            if (!(overlap > MYFILTER18_MIN_SPACE_OVERLAP))
                return false;
        }
    }

    // calc color overlap
    if (this->lowValue <= interval.lowValue)
    {
        if (this->highValue < interval.highValue)
        {
            double overlap = (double ) (this->highValue - interval.lowValue) / (double ) (this->highValue - this->lowValue);

            if (!(overlap > MYFILTER18_MIN_COLOR_OVERLAP))
                return false;
        }
    } else {
        if (this->highValue > interval.highValue)
        {
            double overlap = (double ) (interval.highValue - this->lowValue) / (double ) (this->highValue - this->lowValue);

            if (!(overlap > MYFILTER18_MIN_COLOR_OVERLAP))
                return false;
        }
    }

    this->ttl = MYFILTER18_DEFAULT_TTL;

    this->firstRow = interval.firstRow;
    this->lastRow = interval.lastRow;
    this->lowValue = interval.lowValue;
    this->highValue = interval.highValue;

    this->points.push_back(interval.points[0]);

    return true;
}

#define MYFILTER18_DRAW_COUNTOUR_SKIP 3

void Interval::draw(Mat &src)
{
    int p = 0;
    int s = (int ) points.size();

    while( p < s - MYFILTER18_DRAW_COUNTOUR_SKIP)
    {
        line(src, points[p], points[p + MYFILTER18_DRAW_COUNTOUR_SKIP], Scalar(direction ? 255 : 128));
        p += MYFILTER18_DRAW_COUNTOUR_SKIP;
        //src.at<uchar>(points[p].y, points[p].x) = direction ? 255 : 128;
    }

    if (p != s)
        line(src, points[p - MYFILTER18_DRAW_COUNTOUR_SKIP], points[s - 1], Scalar(direction ? 255 : 128));
}

Mat myfilter18(Mat src, int thres, int thres2, int thres3, int bKsize)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2.0);

    //equalizeHist(gray, gray);

    if (!src.isContinuous())
        return gray;

    vector<Interval> incIntervals;
    vector<Interval> decIntervals;

    //print_values(gray);

    try
    {
        int nr = src.rows;
        int nc = src.cols;

        uchar *srow = gray.ptr<uchar>(0);
        //uchar *frow = flt.ptr<uchar>(0);

        int i = 0;

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            int prec;
            int cur = srow[c];

            i = c;

            int start = -1;
            int startRow = -1;
            uchar startValue = 0;
            uchar maxGrad = 0;
            int maxGrafrow = -1;

            // dir == true  ===> increasing
            // dir == false ===> decreasing
            bool dir = false;

            for (int r = 0; r < nr; r++)
            {
                prec = cur;
                cur = srow[i];

                i += nc;

                if (cur >= prec)
                {
                    // start != -1
                    // dir == increasing
                    // edge reached or the difference between prec and cur is lesser than thres2
                    if (start != -1 && dir && (cur - prec < thres2 || r == nr - 1))
                    {
                        int diff = prec - startValue;

                        if (diff > thres && maxGrad > thres3)
                        {
                            Interval interval;

                            interval.direction = dir;
                            interval.lowValue = startValue;
                            interval.highValue = prec;
                            interval.firstRow = startRow;
                            interval.lastRow = r - 1;

                            interval.points.push_back(Point(c, maxGrafrow));

                            bool merged = false;

                            for (int v = 0; v < (int ) incIntervals.size(); v++)
                            {
                                if (incIntervals[v].merge(interval))
                                {
                                    merged = true;
                                    break;
                                }
                            }

                            if (!merged)
                                incIntervals.push_back(interval);
                        }

                        start = -1;
                    }

                    if (!dir && start != -1)
                    {
                        // stop decreasing
                        int diff = startValue - prec;

                        if (diff > thres && maxGrad > thres3)
                        {
                            Interval interval;

                            interval.direction = dir;
                            interval.lowValue = prec;
                            interval.highValue = startValue;
                            interval.firstRow = startRow;
                            interval.lastRow = r - 1;

                            interval.points.push_back(Point(c, maxGrafrow));

                            bool merged = false;

                            for (int v = 0; v < (int ) decIntervals.size(); v++)
                            {
                                if (decIntervals[v].merge(interval))
                                {
                                    merged = true;
                                    break;
                                }
                            }

                            if (!merged)
                                decIntervals.push_back(interval);
                        }

                        start = -1;
                    }

                    if (start == -1 && cur - prec >= thres2)
                    {
                        start = i - nc - nc;
                        startRow = r - 1;
                        startValue = prec;
                        dir = true;
                        maxGrafrow = r -1;
                        maxGrad = cur - prec;
                    }

                    if (start != -1 && maxGrad < cur - prec)
                    {
                        maxGrafrow = r - 1;
                        maxGrad = cur - prec;
                    }
                }
                else
                {
                    // start != -1
                    // dir == decreasing
                    // edge reached or the difference between prec and cur is lesser thres2
                    if (start != -1 && !dir && (prec - cur < thres2 || r == nr - 1))
                    {
                        int diff = startValue - prec;

                        if (diff > thres && maxGrad > thres3)
                        {
                            Interval interval;

                            interval.direction = dir;
                            interval.lowValue = prec;
                            interval.highValue = startValue;
                            interval.firstRow = startRow;
                            interval.lastRow = r - 1;

                            interval.points.push_back(Point(c, maxGrafrow));

                            bool merged = false;

                            for (int v = 0; v < (int ) decIntervals.size(); v++)
                            {
                                if (decIntervals[v].merge(interval))
                                {
                                    merged = true;
                                    break;
                                }
                            }

                            if (!merged)
                                decIntervals.push_back(interval);
                        }

                        start = -1;
                    }

                    if (dir && start != -1)
                    {
                        // stop increasing
                        int diff = prec - startValue;

                        if (diff > thres && maxGrad > thres3)
                        {
                            Interval interval;

                            interval.direction = dir;
                            interval.lowValue = startValue;
                            interval.highValue = prec;
                            interval.firstRow = startRow;
                            interval.lastRow = r - 1;

                            interval.points.push_back(Point(c, maxGrafrow));

                            bool merged = false;

                            for (int v = 0; v < (int ) incIntervals.size(); v++)
                            {
                                if (incIntervals[v].merge(interval))
                                {
                                    merged = true;
                                    break;
                                }
                            }

                            if (!merged)
                                incIntervals.push_back(interval);
                        }

                        start = -1;
                    }

                    if (start == -1 && prec - cur >= thres2)
                    {
                        start = i - nc - nc;
                        startRow = r - 1;
                        startValue = prec;
                        dir = false;
                        maxGrafrow = r -1;
                        maxGrad = prec - cur;
                    }

                    if (start != -1 && maxGrad < prec - cur)
                    {
                        maxGrafrow = r - 1;
                        maxGrad = prec - cur;
                    }
                }
            }

            for (int k = 0; k < (int ) incIntervals.size(); k++)
            {
                if (incIntervals[k].ttl > 0)
                    incIntervals[k].ttl--;
                else
                {
                    if (incIntervals[k].points.size() > MYFILTER18_MIN_CONTOUR_SIZE && incIntervals[k].points.size() < MYFILTER18_MAX_CONTOUR_SIZE)
                        incIntervals[k].draw(flt);

                    incIntervals.erase(incIntervals.begin() + k);
                }
            }

            for (int k = 0; k < (int ) decIntervals.size(); k++)
            {
                if (decIntervals[k].ttl > 0)
                    decIntervals[k].ttl--;
                else
                {
                    if (decIntervals[k].points.size() > MYFILTER18_MIN_CONTOUR_SIZE && decIntervals[k].points.size() < MYFILTER18_MAX_CONTOUR_SIZE)
                        decIntervals[k].draw(flt);

                    decIntervals.erase(decIntervals.begin() + k);
                }
            }
        }

        for (int k = 0; k < (int ) incIntervals.size(); k++)
        {
            if (incIntervals[k].points.size() > MYFILTER18_MIN_CONTOUR_SIZE && incIntervals[k].points.size() < MYFILTER18_MAX_CONTOUR_SIZE)
                incIntervals[k].draw(flt);
        }

        for (int k = 0; k < (int ) decIntervals.size(); k++)
        {
            if (decIntervals[k].points.size() > MYFILTER18_MIN_CONTOUR_SIZE && decIntervals[k].points.size() < MYFILTER18_MAX_CONTOUR_SIZE)
                decIntervals[k].draw(flt);
        }

    } catch( Exception &e ) {

        cerr << "Exception (line " << e.line << ") : " << e.msg << endl;
        throw e;
    }

    namedWindow( "gaussian", WINDOW_AUTOSIZE );
    imshow("gaussian", gray);

    imshow("flt", flt);

    return flt;
}

#define INTERVAL2_DEFAULT_TTL 3
#define INTERVAL2_MIN_COLOR_OVERLAP 0.5
#define INTERVAL2_DRAW_COUNTOUR_SKIP 2

class Interval2
{
public:
    uchar highValue;
    uchar lowValue;

    int row;
    int col;

    bool direction;
    int ttl;

    vector<Point> points;

    Interval2();
    bool merge(Interval2 &interval);
    void draw(Mat &src);
};

Interval2::Interval2()
{
    ttl = INTERVAL2_DEFAULT_TTL;
}

bool Interval2::merge(Interval2 &interval)
{
    // check cols
    if (this->col == interval.col)
        return false;

    // check space distance
    if (abs(this->row - interval.row) > 2)
        return 0.0;

    // check color overlap
    if (this->lowValue > interval.highValue || this->highValue < interval.lowValue)
        return 0.0;

    int thres = max((this->highValue - this->lowValue) * 3 / 4, 1);

    int d1 = abs((int ) this->lowValue - (int ) interval.lowValue);
    int d2 = abs((int ) this->highValue - (int ) interval.highValue);



    // calc color overlap
    if (d1 > thres || d2 > thres)
        return false;

    this->ttl = INTERVAL2_DEFAULT_TTL;

    this->row = interval.row;
    this->lowValue = interval.lowValue;
    this->highValue = interval.highValue;

    this->points.push_back(interval.points[0]);

    return true;
}

void Interval2::draw(Mat &src)
{
    int p = 0;
    int s = (int ) points.size();

    while( p < s - INTERVAL2_DRAW_COUNTOUR_SKIP)
    {
        line(src, points[p], points[p + INTERVAL2_DRAW_COUNTOUR_SKIP], Scalar(direction ? 255 : 128));
        p += INTERVAL2_DRAW_COUNTOUR_SKIP;
        //src.at<uchar>(points[p].y, points[p].x) = direction ? 255 : 128;
    }

    if (p != s)
        line(src, points[p - INTERVAL2_DRAW_COUNTOUR_SKIP], points[s - 1], Scalar(direction ? 255 : 128));
}

#define MYFILTER19_MIN_CONTOUR_SIZE 10
#define MYFILTER19_MAX_CONTOUR_SIZE 3000

Mat myfilter19(Mat src, int thres, int bKsize)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2.0);

    //equalizeHist(gray, gray);

    if (!src.isContinuous())
        return gray;

    vector<Interval2> incIntervals;
    vector<Interval2> decIntervals;

    //print_values(gray);

    try
    {
        int nr = src.rows;
        int nc = src.cols;

        uchar *srow = gray.ptr<uchar>(0);

        int i = 0;

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            int prec;
            int cur = srow[c];

            i = c;

            for (int r = 0; r < nr; r++)
            {
                prec = cur;
                cur = srow[i];

                i += nc;

                int diff = cur - prec;

                if (diff >= thres)
                {
                    Interval2 interval;

                    interval.direction = true;
                    interval.lowValue = prec;
                    interval.highValue = cur;
                    interval.row = r;
                    interval.col = c;

                    interval.points.push_back(Point(c, r));

                    bool merged = false;

                    for (int v = 0; v < (int ) incIntervals.size(); v++)
                    {
                        if (incIntervals[v].merge(interval))
                        {
                            merged = true;
                            break;
                        }
                    }

                    if (!merged)
                        incIntervals.push_back(interval);

                } else if (-diff >= thres) {
                    Interval2 interval;

                    interval.direction = false;
                    interval.lowValue = cur;
                    interval.highValue = prec;
                    interval.row = r - 1;
                    interval.col = c;

                    interval.points.push_back(Point(c, r - 1));

                    bool merged = false;

                    for (int v = 0; v < (int ) decIntervals.size(); v++)
                    {
                        if (decIntervals[v].merge(interval))
                        {
                            merged = true;
                            //break;
                        }
                    }

                    if (!merged)
                        decIntervals.push_back(interval);
                }
            }

            for (int k = 0; k < (int ) incIntervals.size(); k++)
            {
                if (incIntervals[k].ttl > 0)
                    incIntervals[k].ttl--;
                else
                {
                    if (incIntervals[k].points.size() > MYFILTER19_MIN_CONTOUR_SIZE && incIntervals[k].points.size() < MYFILTER19_MAX_CONTOUR_SIZE)
                        incIntervals[k].draw(flt);

                    incIntervals.erase(incIntervals.begin() + k);
                }
            }

            for (int k = 0; k < (int ) decIntervals.size(); k++)
            {
                if (decIntervals[k].ttl > 0)
                    decIntervals[k].ttl--;
                else
                {
                    if (decIntervals[k].points.size() > MYFILTER19_MIN_CONTOUR_SIZE && decIntervals[k].points.size() < MYFILTER19_MAX_CONTOUR_SIZE)
                        decIntervals[k].draw(flt);

                    decIntervals.erase(decIntervals.begin() + k);
                }
            }
        }

        for (int k = 0; k < (int ) incIntervals.size(); k++)
        {
            if (incIntervals[k].points.size() > MYFILTER19_MIN_CONTOUR_SIZE && incIntervals[k].points.size() < MYFILTER19_MAX_CONTOUR_SIZE)
                incIntervals[k].draw(flt);
        }

        for (int k = 0; k < (int ) decIntervals.size(); k++)
        {
            if (decIntervals[k].points.size() > MYFILTER19_MIN_CONTOUR_SIZE && decIntervals[k].points.size() < MYFILTER19_MAX_CONTOUR_SIZE)
                decIntervals[k].draw(flt);
        }

    } catch( Exception &e ) {

        cerr << "Exception (line " << e.line << ") : " << e.msg << endl;
        throw e;
    }

    namedWindow( "gaussian", WINDOW_AUTOSIZE );
    imshow("gaussian", gray);

    imshow("flt", flt);

    return flt;
}

Mat myfilter20(Mat src, int thres, int thres2, int bKsize)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (bKsize % 2 == 0)
        bKsize++;

    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2.0);

    //equalizeHist(gray, gray);

    if (!src.isContinuous())
        return gray;

    //print_values(gray);

    try
    {
        int nr = src.rows;
        int nc = src.cols;

        uchar *srow = gray.ptr<uchar>(0);
        uchar *frow = flt.ptr<uchar>(0);

        int i = 0;

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            int prec;
            int cur = srow[c];

            i = c + nc * thres2;

            int imax = -1;
            uchar vmax = 0;
            int smax = 0;

            for (int r = thres2; r < nr; r++)
            {
                prec = srow[i - nc * thres2];
                cur = srow[i];

                i += nc;

                int diff = cur - prec;
                int sgn = (diff > 0) - (diff < 0);

                if (diff * sgn <= thres)
                    continue;

                if (imax != -1)
                {
                    if (sgn == smax && diff * sgn <= vmax)
                        continue;

                    if ((i - imax) / nc > thres2)
                        frow[imax -  (1 + thres2 / 2) * nc] = 128 + (1 + smax) * 64 - 1;

                    imax = -1;
                }

                if (imax == -1)
                {
                    imax = i - nc;
                    vmax = diff * sgn;
                    smax = sgn;
                }
            }
        }

        /*Mat line = Mat::ones(1,3,CV_8UC1);
        // now apply the morphology close operation
        morphologyEx(flt, flt, MORPH_ERODE, line, Point(-1, -1), 2);
        morphologyEx(flt, flt, MORPH_DILATE, line, Point(-1, -1), 3);*/
    } catch( Exception &e ) {

        cerr << "Exception (line " << e.line << ") : " << e.msg << endl;
        throw e;
    }

    namedWindow( "gaussian", WINDOW_AUTOSIZE );
    imshow("gaussian", gray);

    imshow("flt", flt);

    return flt;
}

Mat myfilter21(Mat image, int thres1, int thres2, int thres3, int thres4, int times, int ksize)
{
    Mat gray;
    cvtColor(image, gray, COLOR_RGB2GRAY);

    ksize = ksize % 2 == 0 ? ksize + 1 : ksize;
    GaussianBlur(gray, gray, Size(ksize, ksize), 2, 2);
    //medianBlur(gray, gray, ksize);
    //Mat gray1 = gray.clone();
    //bilateralFilter (gray, gray1, ksize, 80, 80);
    //gray = gray1;

    try
    {
        if (!image.isContinuous())
            return gray;

        int nr = image.rows;
        int nc = image.cols;

        Mat modules = cv::Mat::zeros(image.size(), CV_8UC1);
        Mat signes = cv::Mat::zeros(image.size(), CV_8UC1);

        uchar *drow = gray.ptr(0);
        uchar *mrow = modules.ptr(0);
        uchar *srow = signes.ptr(0);

        int i = 0;

        // print pixel (gray) values
        /*for (int r = 0; r < nr; r++)
        {
            std::cout << std::endl;
            for (int c = 0; c < nc; c++)
            {
                int cur = drow[i];

                if (cur >= 0)
                {
                    if (cur < 10)
                        std::cout << "    " << cur;
                    else if (cur < 100)
                        std::cout << "   " << cur;
                    else
                        std::cout << "  " << cur;
                } else {
                    if (cur > -10)
                        std::cout << "   " << cur;
                    else if (cur > -100)
                        std::cout << "  " << cur;
                    else
                        std::cout << " " << cur;
                }

                i++;
            }
        }

        std::cout << std::endl;
        std::cout << std::endl;*/

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            i = nc + c;

            int start = -1;
            int sgn = 0;

            int maxi = -1;
            int maxDiff = -1;
            int precDiff = -1;
            bool isClimbingAgain = false;

            int prec = 0;
            int cur = drow[c];
            int next = drow[i];

            for (int r = 1; r < nr - 1; r++)
            {
                i += nc;

                prec = cur;
                cur = next;
                next = drow[i];

                // diffc * sgn == abs(diffc) <=> signum(diffc) == signum(sgn)
                int diffc = cur - prec;
                if (start != -1                         // we we're climbing and...
                        && (diffc * sgn < 0             // the slope has changed...
                            || diffc * sgn < thres1     // or is too small...
                            || isClimbingAgain))        // rollercoaster! :)
                {
                    //int diff = std::min(maxDiff >> 2, 31) << 3;
                    int diff = (prec - start) * sgn;

                    // MSB 1st ==> module
                    //     2nd ==> module
                    //     3rd ==> module
                    //     4th ==> module
                    //     5th ==> module
                    //
                    //     6th ==> direction and luminance code
                    //     7th ==> direction and luminance code
                    //     8th ==> direction and luminance code
                    //
                    if (diff >= thres2)
                    {
                        srow[maxi] = (uchar ) ((sgn + 1) / 2);
                        mrow[maxi] = (uchar ) min(diff, 31);
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    if (diffc != 0 && abs(diffc) >= thres1)
                    {
                        start = prec;
                        sgn = (diffc > 0) - (diffc < 0);
                        maxDiff = diffc * sgn;
                        maxi = i - nc;
                        precDiff = maxDiff;
                        isClimbingAgain = false;
                    }

                    continue;
                }

                diffc = next - prec;

                if (diffc * sgn >= maxDiff)
                {
                    maxi = i - nc;
                    maxDiff = diffc * sgn;
                } else if (diffc > precDiff)
                    isClimbingAgain = true;

                precDiff = diffc;
            }
        }

        // horizontal scan (vertical segments)
        for (int r = 0; r < nr; r++)
        {
            i = r * nc + 1;
            int start = -1;
            int sgn = 0;

            int maxi = -1;
            int maxDiff = -1;
            int precDiff = -1;
            bool isClimbingAgain = false;

            int prec = 0;
            int cur = drow[i - 1];
            int next = drow[i];

            for (int c = 1; c < nc - 1; c++)
            {
                i++;

                prec = cur;
                cur = next;
                next = drow[i];

                // diffc * sgn == abs(diffc) <=> signum(diffc) == signum(sgn)
                int diffc = cur - prec;
                if (start != -1                         // we we're climbing and...
                        && (diffc * sgn < 0             // the slope has changed...
                            || diffc * sgn < thres1     // or is too small...
                            || isClimbingAgain))        // rollercoaster! :)
                {
                    //int diff = std::min(maxDiff >> 2, 31) << 3;
                    int diff = (prec - start) * sgn;

                    // MSB 1st ==> module
                    //     2nd ==> module
                    //     3rd ==> module
                    //     4th ==> module
                    //     5th ==> module
                    //
                    //     6th ==> direction and luminance code
                    //     7th ==> direction and luminance code
                    //     8th ==> direction and luminance code
                    //
                    // DIRECTION AND LUMINANCE CODE
                    //
                    //   1 => HOR - DEC
                    //   2 => HOR - INC
                    //   3 => VER - DEC
                    //   4 => VER - INC
                    //   5 => HOR - DEC + VER - DEC
                    //   6 => HOR - DEV + VER - INC
                    //   7 => HOR - INC + VER - DEC
                    //   8 => HOR - INC + VER - INC
                    //
                    // NOTE: a pixel is set iff module > 0
                    if (diff >= thres2)
                    {
                        int h = mrow[maxi];

                        if (h > 0)
                        {
                            // encoding is straightforward: take the h code and double it (if 0 no displacement of course)
                            // then move the sgn from [-1,1] to [0,1] and sum 3 to get the vertical code;
                            // dec by 1 to get values in range 0-7 and shift of 5 bits
                            srow[maxi] = (uchar ) (2 * (srow[maxi] + 1) + 2 + (sgn + 1) / 2);
                            mrow[maxi] = (uchar ) (max((int ) mrow[maxi], (int ) min(diff >> 2, 31)));
                        }
                        else
                        {
                            srow[maxi] = (uchar ) ((sgn + 1) / 2  + 2);
                            mrow[maxi] = (uchar ) min(diff, 31);
                        }
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    if (diffc != 0 && abs(diffc) >= thres1)
                    {
                        start = prec;
                        sgn = (diffc > 0) - (diffc < 0);
                        maxDiff = diffc * sgn;
                        maxi = i - 1;
                        precDiff = -1;
                        isClimbingAgain = false;
                    }

                    continue;
                }

                diffc = (next - prec) * sgn;

                if (diffc >= maxDiff)
                {
                    maxi = i - 1;
                    maxDiff = diffc;
                } else if (diffc > precDiff)
                    isClimbingAgain = true;

                precDiff = diffc;
            }
        }

        /*for (int r = 0; r < height; r++)
        {
            for (int c = 0; c < width; c++)
            {
                i = (1 * height + r) * nc + 0 * width + c;
                short d = drow[i];
                if (d >= 0)
                {
                    if (d < 10)
                        std::cout << "    " << d;
                    else if (d < 100)
                        std::cout << "   " << d;
                    else
                        std::cout << "  " << d;
                } else {
                    if (d > -10)
                        std::cout << "   " << d;
                    else if (d > -100)
                        std::cout << "  " << d;
                    else
                        std::cout << " " << d;
                }
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;

        for (int r = 0; r < height; r++)
        {
            for (int c = 0; c < width; c++)
            {
                i = (1 * height + r) * nc + 0 * width + c;
                short d = frow[i];
                if (d >= 0)
                {
                    if (d < 10)
                        std::cout << "    " << d;
                    else if (d < 100)
                        std::cout << "   " << d;
                    else
                        std::cout << "  " << d;
                } else {
                    if (d > -10)
                        std::cout << "   " << d;
                    else if (d > -100)
                        std::cout << "  " << d;
                    else
                        std::cout << " " << d;
                }
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;*/

        Mat mask;
        Mat modulesMasked;
        Mat fltSignes;
        Mat element = getStructuringElement(MORPH_RECT, Size(3, 3));

        threshold(modules, modulesMasked, thres3, 255, CV_THRESH_TOZERO);

        times = max(times, 1);

        for (int i = 0; i < times; i++)
        {
            dilate(modulesMasked, mask, element, Point(-1,-1), 1);

            modules.copyTo(modulesMasked, mask);
            threshold(modulesMasked, modulesMasked, thres4, 255, CV_THRESH_TOZERO);

            thres4 = max(thres4 - 2, thres2);
        }

        Mat fltModules = modulesMasked.clone() * 8;

        signes.copyTo(fltSignes, fltModules);

        Mat flt = fltSignes + fltModules;

        namedWindow("mask", WINDOW_AUTOSIZE);
        threshold(mask, mask, 0, 255, CV_THRESH_BINARY);
        imshow("mask", mask * 6);

        namedWindow("modulesMasked", WINDOW_AUTOSIZE);
        imshow("modulesMasked", modulesMasked);

        namedWindow("image", WINDOW_AUTOSIZE);
        imshow("image", image);

        Mat fltBin;
        threshold(flt, fltBin, thres2, 255, CV_THRESH_BINARY);

        namedWindow("fltBin", WINDOW_AUTOSIZE);
        imshow("fltBin", fltBin);

        imshow("flt", flt);

        return flt;

    } catch( Exception &e ) {

        throw e;
    }
}

Mat myfilter22(Mat image, int thres1, int thres2, int thres3, int thres4, int times, int ksize)
{
    Mat gray;
    cvtColor(image, gray, COLOR_RGB2GRAY);

    ksize = ksize % 2 == 0 ? ksize + 1 : ksize;

    GaussianBlur(gray, gray, Size(ksize, ksize), 2, 2);

    Mat gaussian = gray.clone();
    namedWindow("gaussian", WINDOW_AUTOSIZE);
    imshow("gaussian", gaussian);

    try
    {
        if (!image.isContinuous())
            return gray;

        int nr = image.rows;
        int nc = image.cols;

        Mat horizontalModules = cv::Mat::zeros(image.size(), CV_8UC1);
        Mat verticalModules = cv::Mat::zeros(image.size(), CV_8UC1);
        Mat modules = cv::Mat::zeros(image.size(), CV_8UC1);
        Mat signes = cv::Mat::zeros(image.size(), CV_8UC1);

        uchar *drow = gray.ptr(0);
        uchar *hmrow = horizontalModules.ptr(0);
        uchar *vmrow = verticalModules.ptr(0);
        uchar *mrow = modules.ptr(0);
        uchar *srow = signes.ptr(0);

        int i = 0;

        // print pixel (gray) values
        /*for (int r = 0; r < nr; r++)
        {
            std::cout << std::endl;
            for (int c = 0; c < nc; c++)
            {
                int cur = drow[i];

                if (cur >= 0)
                {
                    if (cur < 10)
                        std::cout << "    " << cur;
                    else if (cur < 100)
                        std::cout << "   " << cur;
                    else
                        std::cout << "  " << cur;
                } else {
                    if (cur > -10)
                        std::cout << "   " << cur;
                    else if (cur > -100)
                        std::cout << "  " << cur;
                    else
                        std::cout << " " << cur;
                }

                i++;
            }
        }

        std::cout << std::endl;
        std::cout << std::endl;*/

        // vertical scan (horizontal segments)
        for (int c = 0; c < nc; c++)
        {
            i = nc + c;

            int start = -1;
            int sgn = 0;

            int maxi = -1;
            int maxDiff = -1;
            int precDiff = -1;
            bool isClimbingAgain = false;

            int prec = 0;
            int cur = drow[c];
            int next = drow[i];

            for (int r = 1; r < nr - 1; r++)
            {
                i += nc;

                prec = cur;
                cur = next;
                next = drow[i];

                // diffc * sgn == abs(diffc) <=> signum(diffc) == signum(sgn)
                int diffc = cur - prec;
                if (start != -1                         // we we're climbing and...
                        && (diffc * sgn < 0             // the slope has changed...
                            || diffc * sgn < thres1     // or is too small...
                            || isClimbingAgain))        // rollercoaster! :)
                {
                    //int diff = std::min(maxDiff >> 2, 31) << 3;
                    int diff = (prec - start) * sgn;

                    // MSB 1st ==> module
                    //     2nd ==> module
                    //     3rd ==> module
                    //     4th ==> module
                    //     5th ==> module
                    //
                    //     6th ==> direction and luminance code
                    //     7th ==> direction and luminance code
                    //     8th ==> direction and luminance code
                    //
                    if (diff >= thres2)
                    {
                        srow[maxi] = (uchar ) ((sgn + 1) / 2);
                        mrow[maxi] = (uchar ) min(diff, 31);
                        hmrow[maxi] = mrow[maxi];
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    if (diffc != 0 && abs(diffc) >= thres1)
                    {
                        start = prec;
                        sgn = (diffc > 0) - (diffc < 0);
                        maxDiff = diffc * sgn;
                        maxi = i - nc;
                        precDiff = maxDiff;
                        isClimbingAgain = false;
                    }

                    continue;
                }

                diffc = next - prec;

                if (diffc * sgn >= maxDiff)
                {
                    maxi = i - nc;
                    maxDiff = diffc * sgn;
                } else if (diffc > precDiff)
                    isClimbingAgain = true;

                precDiff = diffc;
            }
        }

        // horizontal scan (vertical segments)
        for (int r = 0; r < nr; r++)
        {
            i = r * nc + 1;
            int start = -1;
            int sgn = 0;

            int maxi = -1;
            int maxDiff = -1;
            int precDiff = -1;
            bool isClimbingAgain = false;

            int prec = 0;
            int cur = drow[i - 1];
            int next = drow[i];

            for (int c = 1; c < nc - 1; c++)
            {
                i++;

                prec = cur;
                cur = next;
                next = drow[i];

                // diffc * sgn == abs(diffc) <=> signum(diffc) == signum(sgn)
                int diffc = cur - prec;
                if (start != -1                         // we we're climbing and...
                        && (diffc * sgn < 0             // the slope has changed...
                            || diffc * sgn < thres1     // or is too small...
                            || isClimbingAgain))        // rollercoaster! :)
                {
                    //int diff = std::min(maxDiff >> 2, 31) << 3;
                    int diff = (prec - start) * sgn;

                    // MSB 1st ==> module
                    //     2nd ==> module
                    //     3rd ==> module
                    //     4th ==> module
                    //     5th ==> module
                    //
                    //     6th ==> direction and luminance code
                    //     7th ==> direction and luminance code
                    //     8th ==> direction and luminance code
                    //
                    // DIRECTION AND LUMINANCE CODE
                    //
                    //   1 => HOR - DEC
                    //   2 => HOR - INC
                    //   3 => VER - DEC
                    //   4 => VER - INC
                    //   5 => HOR - DEC + VER - DEC
                    //   6 => HOR - DEV + VER - INC
                    //   7 => HOR - INC + VER - DEC
                    //   8 => HOR - INC + VER - INC
                    //
                    // NOTE: a pixel is set iff module > 0
                    if (diff >= thres2)
                    {
                        int h = mrow[maxi];

                        vmrow[maxi] = (int ) min(diff, 31);

                        if (h > 0)
                        {
                            // encoding is straightforward: take the h code and double it (if 0 no displacement of course)
                            // then move the sgn from [-1,1] to [0,1] and sum 3 to get the vertical code;
                            // dec by 1 to get values in range 0-7 and shift of 5 bits
                            srow[maxi] = (uchar ) (2 * (srow[maxi] + 1) + 2 + (sgn + 1) / 2);
                            mrow[maxi] = (uchar ) (max(mrow[maxi], vmrow[maxi]));
                        }
                        else
                        {
                            srow[maxi] = (uchar ) ((sgn + 1) / 2  + 2);
                            mrow[maxi] = vmrow[maxi];
                        }
                    }

                    start = -1;
                }

                if (start == -1)
                {
                    if (diffc != 0 && abs(diffc) >= thres1)
                    {
                        start = prec;
                        sgn = (diffc > 0) - (diffc < 0);
                        maxDiff = diffc * sgn;
                        maxi = i - 1;
                        precDiff = -1;
                        isClimbingAgain = false;
                    }

                    continue;
                }

                diffc = (next - prec) * sgn;

                if (diffc >= maxDiff)
                {
                    maxi = i - 1;
                    maxDiff = diffc;
                } else if (diffc > precDiff)
                    isClimbingAgain = true;

                precDiff = diffc;
            }
        }

        /*i = 0;
        for (int r = 0; r < nr; r++)
        {
            for (int c = 0; c < nc; c++)
            {
                short d = drow[i];
                if (d >= 0)
                {
                    if (d < 10)
                        std::cout << "    " << d;
                    else if (d < 100)
                        std::cout << "   " << d;
                    else
                        std::cout << "  " << d;
                } else {
                    if (d > -10)
                        std::cout << "   " << d;
                    else if (d > -100)
                        std::cout << "  " << d;
                    else
                        std::cout << " " << d;
                }

                i++;
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;*/

        /*for (int r = 0; r < height; r++)
        {
            for (int c = 0; c < width; c++)
            {
                i = (1 * height + r) * nc + 0 * width + c;
                short d = frow[i];
                if (d >= 0)
                {
                    if (d < 10)
                        std::cout << "    " << d;
                    else if (d < 100)
                        std::cout << "   " << d;
                    else
                        std::cout << "  " << d;
                } else {
                    if (d > -10)
                        std::cout << "   " << d;
                    else if (d > -100)
                        std::cout << "  " << d;
                    else
                        std::cout << " " << d;
                }
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;*/

        Mat hMask, vMask;
        Mat horizontalModulesMasked, verticalModulesMasked;
        Mat fltSignes;
        Mat element = getStructuringElement(MORPH_RECT, Size(3, 3));

        // horizontal
        Mat hKernel(3, 3, CV_8U, Scalar(0));

        hKernel.at<uchar>(0,0)= 1;
        hKernel.at<uchar>(1,0)= 1;
        hKernel.at<uchar>(2,0)= 1;

        hKernel.at<uchar>(1,1)= 100;

        hKernel.at<uchar>(0,2)= 1;
        hKernel.at<uchar>(1,2)= 1;
        hKernel.at<uchar>(2,2)= 1;

        threshold(horizontalModules, hMask, thres3, 1, CV_THRESH_BINARY);
        // horizontal

        // vertical
        Mat vKernel(3, 3, CV_8U, Scalar(0));

        vKernel.at<uchar>(0,0)= 1;
        vKernel.at<uchar>(0,1)= 1;
        vKernel.at<uchar>(0,2)= 1;

        vKernel.at<uchar>(1,1)= 100;

        vKernel.at<uchar>(2,0)= 1;
        vKernel.at<uchar>(2,1)= 1;
        vKernel.at<uchar>(2,2)= 1;

        threshold(verticalModules, vMask, thres3, 1, CV_THRESH_BINARY);
        // vertical

        times = max(times, 1);

        for (int i = 0; i < times; i++)
        {
            if (i > 0)
                threshold(horizontalModulesMasked, hMask, 0, 1, CV_THRESH_BINARY);
            filter2D(hMask, hMask, hMask.depth(), hKernel);
            threshold(hMask, hMask, 100, 255, CV_THRESH_BINARY);
            dilate(hMask, hMask, element);
            horizontalModules.copyTo(horizontalModulesMasked, hMask);
            threshold(horizontalModulesMasked, horizontalModulesMasked, thres4, 255, CV_THRESH_TOZERO);

            if (i > 0)
                threshold(verticalModulesMasked, vMask, 0, 1, CV_THRESH_BINARY);
            filter2D(vMask, vMask, vMask.depth(), vKernel);
            threshold(vMask, vMask, 100, 255, CV_THRESH_BINARY);
            dilate(vMask, vMask, element);
            verticalModules.copyTo(verticalModulesMasked, vMask);
            threshold(verticalModulesMasked, verticalModulesMasked, thres4, 255, CV_THRESH_TOZERO);

            thres4 = max(thres4 - 2, thres2);
        }

        Mat fltModules = max(horizontalModulesMasked, verticalModulesMasked) * 8;
        //Mat fltModules = verticalModulesMasked * 8;

        signes.copyTo(fltSignes, fltModules);

        Mat flt = fltSignes + fltModules;

        namedWindow("mask", WINDOW_AUTOSIZE);
        Mat mask = max(hMask, vMask);
        threshold(mask, mask, 0, 255, CV_THRESH_BINARY);
        imshow("mask", mask * 6);

        //Mat horizontalModulesBinary;
        //threshold(horizontalModules, horizontalModulesBinary, 0, 255, CV_THRESH_BINARY);
        //namedWindow("horizontalModulesBinary", WINDOW_AUTOSIZE);
        //imshow("horizontalModulesBinary", horizontalModulesBinary);

        //Mat verticalModulesBinary;
        //threshold(verticalModules, verticalModulesBinary, 0, 255, CV_THRESH_BINARY);
        //namedWindow("verticalModulesBinary", WINDOW_AUTOSIZE);
        //imshow("verticalModulesBinary", verticalModulesBinary);

        Mat horizontalModulesMaskedBinary;
        threshold(horizontalModulesMasked, horizontalModulesMaskedBinary, 0, 255, CV_THRESH_BINARY);
        namedWindow("horizontalModulesMaskedBinary", WINDOW_AUTOSIZE);
        imshow("horizontalModulesMaskedBinary", horizontalModulesMaskedBinary);

        Mat verticalModulesMaskedBinary;
        threshold(verticalModulesMasked, verticalModulesMaskedBinary, 0, 255, CV_THRESH_BINARY);
        namedWindow("verticalModulesMaskedBinary", WINDOW_AUTOSIZE);
        imshow("verticalModulesMaskedBinary", verticalModulesMaskedBinary);

        Mat modulesBinary;
        threshold(modules, modulesBinary, 0, 255, CV_THRESH_BINARY);
        namedWindow("modulesBinary", WINDOW_AUTOSIZE);
        imshow("modulesBinary", modulesBinary);

        //namedWindow("image", WINDOW_AUTOSIZE);
        //imshow("image", image);

        Mat fltBin;
        threshold(flt, fltBin, 0, 255, CV_THRESH_BINARY);
        namedWindow("fltBin", WINDOW_AUTOSIZE);
        imshow("fltBin", fltBin);

        imshow("flt", flt);

        return flt;

    } catch( Exception &e ) {

        throw e;
    }
}

Mat myfilter23(Mat src, int morphTypeIndex, int kernelSize, int times, int thres)
{
    Mat gray;
    Mat flt = cv::Mat::zeros(src.size(), CV_8UC1);

    cvtColor(src, gray, COLOR_RGB2GRAY);

    if (!gray.isContinuous())
        return gray;

    //GaussianBlur(gray, gray, Size(5, 5), 0.0);

    int morphType;

    switch (morphTypeIndex) {
    case 1:
        morphType = MORPH_CROSS;
        break;
    case 2:
        morphType = MORPH_ELLIPSE;
        break;
    default:
        morphType = MORPH_RECT;
        break;
    }

    Mat element = getStructuringElement(morphType,
                                        Size(2 * kernelSize + 1, 2 * kernelSize + 1),
                                        Point(kernelSize, kernelSize));

    Mat er;
    Mat dil;

    int nr = gray.rows;
    int nc = gray.cols;

    flt = gray.clone();

    for (int t = 0; t < times; t++)
    {
        erode(flt, er, element);
        dilate(flt, dil, element);

        uchar *erow = er.ptr(0);
        uchar *drow = dil.ptr(0);
        uchar *frow = flt.ptr(0);

        for (int i = 0; i < nr * nc; i++)
            // frow[i] = drow[i] - frow[i] == frow[i] - erow[i] ? frow[i] : drow[i] - frow[i] < frow[i] - erow[i] ? drow[i] : erow[i];
            frow[i] = drow[i] - frow[i] < frow[i] - erow[i] ? drow[i] : erow[i];
    }

    printf("\n");
    print_values(flt);
    printf("\n");
    print_histogram(flt);
    printf("\n");

    //erode(flt, flt, Mat());
    //dilate(flt, flt, Mat());

    Mat binary;
    threshold(flt, binary, thres, 255, THRESH_BINARY_INV);

    erode(binary, binary, Mat());
    dilate(binary, binary, Mat());

    namedWindow("binary", WINDOW_AUTOSIZE);
    imshow("binary", binary);

    return flt;
}

Mat test(Mat src, int ksize, int bKsize, int thres, int iter)
{
    if (bKsize % 2 == 0)
        bKsize++;

    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    //medianBlur(gray, gray, bKsize);
    GaussianBlur(gray, gray, Size(bKsize, bKsize), 2.0, 2.0);

    Mat element = getStructuringElement(MORPH_RECT, Size(2 * ksize + 1, 2 * ksize + 1), Point(ksize, ksize));

    Mat dil, ero;

    dilate(gray, dil, element, Point(ksize, ksize), iter);
    erode(gray, ero, element, Point(ksize, ksize), iter);

    gray = gray / 2;
    dil = dil / 2;
    ero = ero / 2;

    Mat flt = gray + ero;

    Mat thresholded;

    threshold(flt, thresholded, thres, 255, THRESH_BINARY);

    imshow("flt", flt);
    imshow("thresholded", thresholded);

    return flt;
}

Mat test2(Mat src, int ksize, int bKsize, int thres)
{
    if (bKsize % 2 == 0)
        bKsize++;

    Mat gray;
    cvtColor(src, gray, COLOR_RGB2GRAY);

    //medianBlur(gray, gray, bKsize);
    //GaussianBlur(gray, gray, Size(bKsize, bKsize), 2.0, 2.0);

    Mat element = getStructuringElement(MORPH_RECT, Size(2 * ksize + 1, 2 * ksize + 1), Point(ksize, ksize));

    Mat dil, ero;

    dilate(gray, dil, element, Point(ksize, ksize));
    erode(gray, ero, element, Point(ksize, ksize));

    Mat flt = dil - ero;

    printf("\n");
    print_values(gray);
    printf("\n");
    /*print_values(dil);
    printf("\n");
    print_values(ero);
    printf("\n");*/
    print_values(flt);
    printf("\n");

    Mat thresholded;

    threshold(flt, thresholded, thres, 255, THRESH_BINARY);

    imshow("flt", flt);
    imshow("thresholded", thresholded);

    return flt;
}

void onTrackbarChange(int , void*)
{
    //Mat flt = minmax(roi, cursor1);
    //Mat flt = minmax2(roi, cursor1, cursor2);
    //Mat flt = sd(roi, cursor1);
    //Mat flt = binary(roi, cursor1, cursor2, cursor3);
    //Mat flt = binaryInv(roi, cursor1, cursor2, cursor3);
    //Mat flt = binaryOTSU(roi, cursor1);
    //Mat flt = laplacianop(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = laplacianop2(roi, cursor1, cursor2);
    //Mat flt = sobelop(roi, cursor1, cursor2, cursor3);
    //Mat flt = cannyop(roi, cursor1, cursor2, cursor3);

    //Mat src(2, 2, CV_16UC1);
    //Mat flt = bitshift(roi, cursor1);

    //Mat flt = myfilter(roi, cursor1, cursor2, cursor3);
    //Mat flt = myfilter2(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter3(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter4(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter5(roi, cursor1, cursor2, cursor3);
    //Mat flt = myfilter6(roi, cursor1, cursor2, cursor3);
    //Mat flt = myfilter7(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter8(roi, cursor1);
    //Mat flt = myfilter9(roi, cursor1, cursor2, cursor3);
    //Mat flt = myfilter10(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter11(roi, cursor1, cursor2, cursor3);
    //Mat flt = myfilter12(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter12s(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter13(roi, cursor1, cursor2);
    //Mat flt = myfilter14(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter15(roi, cursor1, cursor2, cursor3);
    //Mat flt = myfilter16(roi, cursor1, cursor2);
    //Mat flt = myfilter17(roi, cursor1, cursor2, cursor3);
    //Mat flt = myfilter18(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter19(roi, cursor1, cursor2);
    //Mat flt = myfilter20(roi, cursor1, cursor2, cursor3);
    //Mat flt = kmeansop(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = myfilter21(roi, cursor1, cursor2, cursor3, cursor4, cursor5, cursor6);
    //Mat flt = myfilter22(roi, cursor1, cursor2, cursor3, cursor4, cursor5, cursor6);
    Mat flt = myfilter23(roi, cursor1, cursor2, cursor3, cursor4);

    //Mat flt = test(roi, cursor1, cursor2, cursor3, cursor4);
    //Mat flt = test2(roi, cursor1, cursor2, cursor3);

    //std::vector<std::vector<Point> > contours;

    //Mat roic = roi.clone();
    /*findContours(flt,
                 contours,
                 CV_RETR_LIST,
                 CV_CHAIN_APPROX_SIMPLE);

    for (int i = 0; i < (int ) contours.size(); i++)
        approxPolyDP(Mat(contours[i]), contours[i], cursor6, true);

    drawContours(roic,    // image
                 contours,   // contours
                 -1,         // draw all contours
                 255,        // contours color
                 1);         // thickness*/

    //imshow( "contours", roic );

    //imwrite("save.jpg", flt);

    resizeWindow("flt", 480, 420);
    imshow("flt", flt);
}

/**
* @function main
*/
int main()
{
    /// Read the image
    try
    {
        Mat origin = imread("image6.png");

        if (origin.empty())
            return -1;

        //roi = origin(Rect(1120, 195, 20, 55));
        roi = origin;
        //resize(roi, roi, Size(0, 0), 0.4, 0.4, INTER_NEAREST);
    } catch (Exception e) {
        cout << endl << "while reading image: " << e.err << endl;
        return 1;
    }

    /// Create Trackbars for Thresholds
    namedWindow("flt", WINDOW_NORMAL);
    createTrackbar("cursor1", "flt", &cursor1, max_trackbar, onTrackbarChange);
    createTrackbar("cursor2", "flt", &cursor2, max_trackbar, onTrackbarChange);
    createTrackbar("cursor3", "flt", &cursor3, max_trackbar, onTrackbarChange);
    createTrackbar("cursor4", "flt", &cursor4, max_trackbar, onTrackbarChange);
    createTrackbar("cursor5", "flt", &cursor5, max_trackbar, onTrackbarChange);
    createTrackbar("cursor6", "flt", &cursor6, max_trackbar, onTrackbarChange);

    /// Initialize
    onTrackbarChange(0, 0);

    waitKey(0);

    return 0;
}
